package `in`.supplynote.resu.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import `in`.supplynote.resu.R
import `in`.supplynote.resu.databinding.FragmentChatBinding
import `in`.supplynote.resu.ui.main.viewmodel.MainViewModel
import android.util.Log
import android.widget.Toast
import androidx.activity.addCallback
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.getstream.sdk.chat.viewmodel.MessageInputViewModel
import com.getstream.sdk.chat.viewmodel.messages.MessageListViewModel
import dagger.hilt.android.AndroidEntryPoint
import io.getstream.chat.android.client.ChatClient
import io.getstream.chat.android.client.models.Channel
import io.getstream.chat.android.ui.message.input.viewmodel.bindView
import io.getstream.chat.android.ui.message.list.header.viewmodel.MessageListHeaderViewModel
import io.getstream.chat.android.ui.message.list.header.viewmodel.bindView
import io.getstream.chat.android.ui.message.list.viewmodel.bindView
import io.getstream.chat.android.ui.message.list.viewmodel.factory.MessageListViewModelFactory
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class ChatFragment : Fragment() {

    private lateinit var binding: FragmentChatBinding
    private val viewModel: MainViewModel by viewModels()
    private lateinit var navController: NavController
    private lateinit var client: ChatClient
    private lateinit var cid: String
    private lateinit var channelData: Channel
    private var channelDataFetched = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentChatBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        cid = requireArguments().getString("cid", "")
        client = ChatClient.instance()
        bindAndInit()
        getChannelData()

    }

    private fun bindAndInit() {

        binding.messageListHeaderView.hideAvatar()

        val factory = MessageListViewModelFactory(cid)
        val messageListHeaderViewModel: MessageListHeaderViewModel by viewModels { factory }
        val messageListViewModel: MessageListViewModel by viewModels { factory }
        val messageInputViewModel: MessageInputViewModel by viewModels { factory }

        messageListHeaderViewModel.bindView(binding.messageListHeaderView, this)
        messageListViewModel.bindView(binding.messageListView, this)
        messageInputViewModel.bindView(binding.messageInputView, this)

        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)

        binding.messageListView.setMessageEditHandler(messageInputViewModel::postMessageToEdit)

        messageListViewModel.state.observe(viewLifecycleOwner) { state ->
            if (state is MessageListViewModel.State.NavigateUp) {
                activity?.onBackPressed()
            }
        }

        val backHandler = {
            messageListViewModel.onEvent(MessageListViewModel.Event.BackButtonPressed)
        }
        binding.messageListHeaderView.setBackButtonClickListener(backHandler)
        activity?.onBackPressedDispatcher?.addCallback(this) {
            backHandler()
        }
        binding.messageListHeaderView.setTitleClickListener{
            val bundle = bundleOf("cid" to cid)
            navController.navigate(R.id.action_chatFragment_to_editChannelFragment, bundle)
        }

        binding.cart.setOnClickListener {
            if (channelDataFetched) {
                navigateToCreatePurchaseOrder(channelData.extraData["restaurantId"].toString())
            } else {
                // TODO (handle else case)
            }
        }

    }

    private fun getChannelData() {

        val channelClient = client.channel(cid)
        channelClient.watch().enqueue {
            if (it.isSuccess) {
                channelData = it.data()
                channelDataFetched = true
            } else {
                Toast.makeText(requireContext(), it.error().message.toString(), Toast.LENGTH_LONG).show()
            }
        }

    }

    private fun navigateToCreatePurchaseOrder(businessId: String) {
        val bundle = Bundle()
        bundle.putString("businessId", businessId)
        navController.navigate(R.id.action_chatFragment_to_createPurchaseFragment, bundle)
    }

}

/*

Channel(
    cid=messaging:!members-xN73_DGxkeOf9-e7nNHyWqNJzCxTOpj0TX6oIjUPn1w,
    id=!members-xN73_DGxkeOf9-e7nNHyWqNJzCxTOpj0TX6oIjUPn1w,
    type=messaging,
    watcherCount=2,
    frozen=false,
    lastMessageAt=null,
    createdAt=Mon May 09 04:58:59 GMT+05:30 2022,
    deletedAt=null,
    updatedAt=Mon May 09 04:58:59 GMT+05:30 2022,
    syncStatus=COMPLETED,
    memberCount=2,
    messages=[],
    members=[
        Member(
            user=User(
                id=5d808abcb84cd238382b1f04,
                role=user,
                invisible=false,
                banned=false,
                devices=[],
                online=true,
                createdAt=Mon May 09 04:58:26 GMT+05:30 2022,
                updatedAt=Mon May 09 04:58:26 GMT+05:30 2022,
                lastActive=Mon May 09 05:13:30 GMT+05:30 2022,
                totalUnreadCount=0,
                unreadChannels=0,
                mutes=[],
                teams=[],
                channelMutes=[],
                extraData={name=setup3 Admin}
            ),
            role=owner,
            createdAt=Mon May 09 04:58:59 GMT+05:30 2022,
            updatedAt=Mon May 09 04:58:59 GMT+05:30 2022,
            isInvited=null,
            inviteAcceptedAt=null,
            inviteRejectedAt=null,
            shadowBanned=false
        ),
        Member(
            user=User(
                id=611cbb0eaf7a2610a8f2913f,
                role=user,
                invisible=false,
                banned=false,
                devices=[],
                online=false,
                createdAt=Mon May 09 03:56:08 GMT+05:30 2022,
                updatedAt=Mon May 09 03:56:08 GMT+05:30 2022,
                lastActive=Mon May 09 03:56:08 GMT+05:30 2022,
                totalUnreadCount=0,
                unreadChannels=0,
                mutes=[],
                teams=[],
                channelMutes=[],
                extraData={name=freshfoods}
            ),
            role=member,
            createdAt=Mon May 09 04:58:59 GMT+05:30 2022,
            updatedAt=Mon May 09 04:58:59 GMT+05:30 2022,
            isInvited=null,
            inviteAcceptedAt=null,
            inviteRejectedAt=null,
            shadowBanned=false
        )
    ],
    watchers=[],
    read=[
        ChannelUserRead(
            user=User(
                id=5d808abcb84cd238382b1f04,
                role=user,
                invisible=false,
                banned=false, devices=[],
                online=true,
                createdAt=Mon May 09 04:58:26 GMT+05:30 2022,
                updatedAt=Mon May 09 04:58:26 GMT+05:30 2022,
                lastActive=Mon May 09 05:13:30 GMT+05:30 2022,
                totalUnreadCount=0,
                unreadChannels=0,
                mutes=[],
                teams=[],
                channelMutes=[],
                extraData={name=setup3 Admin}
            ),
            lastRead=Mon May 09 04:58:59 GMT+05:30 2022,
            unreadMessages=0
        ),
        ChannelUserRead(
            user=User(
                id=611cbb0eaf7a2610a8f2913f,
                role=user,
                invisible=false,
                banned=false,
                devices=[],
                online=false,
                createdAt=Mon May 09 03:56:08 GMT+05:30 2022,
                updatedAt=Mon May 09 03:56:08 GMT+05:30 2022,
                lastActive=Mon May 09 03:56:08 GMT+05:30 2022,
                totalUnreadCount=0,
                unreadChannels=0,
                mutes=[],
                teams=[],
                channelMutes=[],
                extraData={name=freshfoods}
            ),
            lastRead=Mon May 09 04:58:59 GMT+05:30 2022, unreadMessages=0
        )
    ],
    config=Config(
        createdAt=Mon May 09 03:11:01 GMT+05:30 2022,
        updatedAt=Mon May 09 03:11:01 GMT+05:30 2022,
        name=messaging,
        typingEventsEnabled=true,
        readEventsEnabled=true,
        connectEventsEnabled=true,
        searchEnabled=true,
        isReactionsEnabled=true,
        isThreadEnabled=true,
        muteEnabled=true,
        uploadsEnabled=true,
        urlEnrichmentEnabled=true,
        customEventsEnabled=true,
        pushNotificationsEnabled=true,
        messageRetention=infinite,
        maxMessageLength=5000,
        automod=disabled,
        automodBehavior=flag,
        blocklistBehavior=,
        commands=[
            Command(
                name=giphy,
                description=Post a random gif to the channel,
                args=[text],
                set=fun_set
            )
        ]
    ),
    createdBy=User(
        id=5d808abcb84cd238382b1f04,
        role=user,
        invisible=false,
        banned=false,
        devices=[],
        online=true,
        createdAt=Mon May 09 04:58:26 GMT+05:30 2022,
        updatedAt=Mon May 09 04:58:26 GMT+05:30 2022,
        lastActive=Mon May 09 05:13:30 GMT+05:30 2022,
        totalUnreadCount=0,
        unreadChannels=0,
        mutes=[],
        teams=[],
        channelMutes=[],
        extraData={name=setup3 Admin}
    ),
    unreadCount=0,
    team=,
    extraData={
        disabled=false,
        own_capabilities=[
            connect-events,
            delete-channel,
            delete-own-message,
            flag-message,
            join-channel,
            leave-channel,
            mute-channel,
            pin-message,
            quote-message,
            read-events,
            search-messages,
            send-custom-events,
            send-links,
            send-message,
            send-reaction,
            send-reply,
            send-typing-events,
            typing-events,
            update-channel,
            update-own-message,
            upload-file
        ],
         hidden=false,
         supplierId=5ddbbb047c6ff9993d87d844,
         restaurantId=5d808a85b84cd238382b1f02,
         name=Fresh Foods
     },
     hidden=null,
     hiddenMessagesBefore=null,
     cooldown=0,
     pinnedMessages=[]
 )

 */