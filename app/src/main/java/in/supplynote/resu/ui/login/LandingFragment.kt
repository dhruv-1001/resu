package `in`.supplynote.resu.ui.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import `in`.supplynote.resu.R
import `in`.supplynote.resu.databinding.FragmentLandingBinding
import androidx.navigation.NavController
import androidx.navigation.Navigation
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class LandingFragment : Fragment() {

    private lateinit var binding: FragmentLandingBinding
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_landing, container, false)
        binding = FragmentLandingBinding.bind(view)

        bindAndInit()

        return view
    }

    private fun bindAndInit() {
        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment_container)
        binding.constLoginWithPhone.setOnClickListener {
            navController.navigate(R.id.action_landingFragment_to_phoneLoginFragment)
        }
        binding.constLoginWithSupplynote.setOnClickListener {
            navController.navigate(R.id.action_landingFragment_to_loginWithSupplyNoteFragment)
        }
    }

}