package `in`.supplynote.resu.ui.login

import `in`.supplynote.resu.R
import `in`.supplynote.resu.data.model.Status
import `in`.supplynote.resu.databinding.FragmentLoginWithSupplynoteBinding
import `in`.supplynote.resu.ui.login.viewmodel.LoginViewModel
import `in`.supplynote.resu.ui.main.MainActivity
import `in`.supplynote.resu.utils.d
import `in`.supplynote.resu.utils.showToast
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class LoginWithSupplyNoteFragment : Fragment() {

    private lateinit var binding: FragmentLoginWithSupplynoteBinding
    private val viewModel: LoginViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_login_with_supplynote, container, false)
        binding = FragmentLoginWithSupplynoteBinding.bind(view)
        bindAndInitialise()
        return view

    }

    private fun bindAndInitialise() {
        binding.btLogin.setOnClickListener { login() }
        binding.ivBack.setOnClickListener { activity?.onBackPressed() }
    }

    private fun login() {

        val credentials = JsonObject()
        credentials.addProperty("username", binding.etUserName.text.toString())
        credentials.addProperty("password", binding.etPassword.text.toString())

        viewModel.loginWithSupplyNote(credentials).observe(requireActivity()) { res ->
            when (res.status) {
                Status.LOADING -> {
                    binding.pbLoading.visibility = View.VISIBLE
                }
                Status.SUCCESS -> {
                    binding.pbLoading.visibility = View.INVISIBLE
                    loginUser(res.data!!)
                }
                Status.ERROR -> {
                    binding.pbLoading.visibility = View.INVISIBLE
                    d(res.data.toString())
                    showToast(res.msg.toString())
                }
            }
        }
    }

    private fun loginUser(data: JsonElement) {

        val intent = Intent(requireContext(), MainActivity::class.java)
        intent.putExtra("userName", data.asJsonObject.get("name").asString)
        intent.putExtra("userId", data.asJsonObject.get("_id").asString)

        startActivity(intent)
        activity?.finish()

    }


}