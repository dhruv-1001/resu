package `in`.supplynote.resu.ui.main

import `in`.supplynote.resu.R
import `in`.supplynote.resu.databinding.FragmentCreateChannelBinding
import `in`.supplynote.resu.ui.main.adapters.MembersShowAdapter
import `in`.supplynote.resu.ui.main.viewmodel.MainViewModel
import `in`.supplynote.resu.utils.*
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.gson.JsonArray
import dagger.hilt.android.AndroidEntryPoint
import io.getstream.chat.android.client.ChatClient
import kotlinx.coroutines.ExperimentalCoroutinesApi
import timber.log.Timber


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class CreateChannelFragment : Fragment() {

    private lateinit var binding: FragmentCreateChannelBinding

    private val viewModel: MainViewModel by activityViewModels()
    private lateinit var navController: NavController
    private lateinit var businessArray: ArrayList<String>
    private lateinit var businessJsonArray: JsonArray

    private lateinit var teamMembersShowAdapter: MembersShowAdapter
    private lateinit var otherMembersShowAdapter: MembersShowAdapter

    private lateinit var chatClient: ChatClient

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_create_channel, container, false)
        binding = FragmentCreateChannelBinding.bind(view)

        chatClient = ChatClient.instance()
        bindAndInit()

        return view

    }

    private fun bindAndInit() {

        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)

        binding.addPersonConst.setOnClickListener {
            setSelectedBusinessId()
            navigateToAddMember(false)
        }
        binding.addTeamPersonConst.setOnClickListener { navigateToAddMember(true) }

        binding.ivBack.setOnClickListener { activity?.onBackPressed() }

        if (viewModel.businessType == BusinessType.RESTAURANT) {
            binding.businessTypeHeading.text = "Supplier"
            binding.otherBusinessAddParticipant.text = "Add supplier participant"
            binding.selfBusinessAddParticipant.text = "Add restaurant participant"
            binding.userBusinessType.text = "Restaurant"
            binding.userPhoneNumber.text = viewModel.phoneNumber
        } else {
            binding.businessTypeHeading.text = "Restaurant"
            binding.otherBusinessAddParticipant.text = "Add restaurant participant"
            binding.selfBusinessAddParticipant.text = "Add supplier participant"
            binding.userBusinessType.text = "Restaurant"
            binding.userPhoneNumber.text = viewModel.phoneNumber
        }

        binding.userName.text = viewModel.userName
        binding.usernameInitial.text = viewModel.userName[0].uppercaseChar().toString()

        setupSpinner()

        binding.btCreateChannel.setOnClickListener {
            if (binding.etChannelName.text.isEmpty()) {
                fireSnackbar(
                    it,
                    SnackbarLevel.WARNING,
                    "Enter Channel Name"
                )
            } else {
                createChannel()
            }
        }

    }

    private fun setSelectedBusinessId() {
        val selectedBusinessName = binding.spBusiness.selectedItem.toString()
        for (x in businessJsonArray) {
            if (x.asJsonObject.get("businessName").asString.equals(selectedBusinessName)){
                viewModel.setSelectedOtherBusinessId(x.asJsonObject.get("_id").asString)
                break
            }
        }
    }

    private fun createChannel() {

        val selectedTeamMembers = viewModel.selectedTeamMembersIds.value!!
        selectedTeamMembers.add(viewModel.userId)
        val selectedOtherMembers = viewModel.selectedOtherMembersIds.value!!

        val allSelectedIds = ArrayList<String>()
        allSelectedIds.addAll(selectedOtherMembers)
        allSelectedIds.addAll(selectedTeamMembers)
        Log.d("ALL MEMBERS", allSelectedIds.toString())

        var restaurantId = ""
        var supplierId = ""

        if (viewModel.businessType == BusinessType.RESTAURANT) {
            restaurantId = viewModel.businessId
            supplierId = viewModel.getSelectedOtherBusinessId()
        } else {
            supplierId = viewModel.businessId
            restaurantId = viewModel.getSelectedOtherBusinessId()
        }

        chatClient.createChannel(
            channelType = "messaging",
            members = allSelectedIds.toList(),
            extraData = mutableMapOf(
                "name" to binding.etChannelName.text.toString(),
                "restaurantId" to restaurantId,
                "supplierId" to supplierId
            ),
        ).enqueue{
            if (it.isSuccess) {
                showToast("Channel Created")
                binding.ivBack.performClick()
            } else {
                showToast(it.error().toString())
            }
        }

    }


    private fun navigateToAddMember(type: Boolean) {
        viewModel.isTeamMembersData = type
        navController.navigate(R.id.action_createChannelFragment_to_addMemberFragment)
    }

    private fun setupSpinner() {

        businessArray = if (viewModel.businessType == BusinessType.RESTAURANT) {
            viewModel.getBusinessArrayList("sellerBusinesses")
        } else {
            viewModel.getBusinessArrayList("buyerBusinesses")
        }

        businessJsonArray = if (viewModel.businessType == BusinessType.RESTAURANT) {
            viewModel.getBusinessJsonArray("sellerBusinesses")
        } else {
            viewModel.getBusinessJsonArray("buyerBusinesses")
        }
        d(businessArray.toString())
        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            businessArray
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spBusiness.adapter = adapter

    }

    override fun onResume() {
        super.onResume()

        if (viewModel.teamMembersSelected) {
            binding.rvSelectedTeamMembers.setHasFixedSize(true)
            teamMembersShowAdapter = MembersShowAdapter(
                viewModel.getSelectedTeamMembersJson()
            )
            binding.rvSelectedTeamMembers.adapter = teamMembersShowAdapter
            teamMembersShowAdapter.updateRecycler()
        }
        if (viewModel.otherMembersSelected) {
            binding.rvSelectedOtherMembers.setHasFixedSize(true)
            otherMembersShowAdapter = MembersShowAdapter(
                viewModel.getSelectedOtherMembersJson()
            )
            binding.rvSelectedOtherMembers.adapter = otherMembersShowAdapter
            otherMembersShowAdapter.updateRecycler()
        }

    }

}