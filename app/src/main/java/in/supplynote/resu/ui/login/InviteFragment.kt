package `in`.supplynote.resu.ui.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import `in`.supplynote.resu.R
import `in`.supplynote.resu.data.model.Status
import `in`.supplynote.resu.databinding.FragmentInviteBinding
import `in`.supplynote.resu.ui.login.viewmodel.LoginViewModel
import android.app.AlertDialog
import android.content.Context
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.gson.JsonObject
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class InviteFragment : Fragment() {

    private lateinit var binding: FragmentInviteBinding
    private lateinit var navController: NavController

    private val viewModel: LoginViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_invite, container, false)
        binding = FragmentInviteBinding.bind(view)

        bindAndInit()

        return view
    }

    private fun bindAndInit() {
        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment_container)
        binding.joinOrg.setOnClickListener {
            navigateToUserSignup()
        }
    }

    private fun navigateToUserSignup() {
        navController.navigate(R.id.action_inviteFragment_to_userSignupFragment)
    }

    private fun showAlertToExit() {

        AlertDialog.Builder(requireContext())
            .setTitle("Close Application")
            .setMessage("Are you sure you want to exit Resu?")
            .setPositiveButton("Yes") { _, _ -> activity?.finish() }
            .setNegativeButton("No", null)
            .show()

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                showAlertToExit()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }


}