package `in`.supplynote.resu.ui.main

import `in`.supplynote.resu.R
import `in`.supplynote.resu.databinding.FragmentEditChannelBinding
import `in`.supplynote.resu.ui.chat.ChatActivity
import `in`.supplynote.resu.ui.main.adapters.ChannelMembersAdapter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.getstream.chat.android.client.ChatClient
import io.getstream.chat.android.client.channel.ChannelClient
import io.getstream.chat.android.client.models.Channel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class EditChannelFragment : Fragment() {

    private lateinit var binding: FragmentEditChannelBinding
    private lateinit var cid: String
    private lateinit var userId: String

    private lateinit var client: ChatClient
    private lateinit var channel: Channel
    private lateinit var channelClient: ChannelClient

    private lateinit var adapter: ChannelMembersAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_edit_channel, container, false)
        binding = FragmentEditChannelBinding.bind(view)

        client = ChatClient.instance()

        val activity: ChatActivity = activity as ChatActivity
        cid = activity.getCid()
        userId = activity.getUid()

        channelClient = client.channel(cid)

        bindAndInit()
        getChannelData()

        return view

    }

    private fun bindAndInit() {

        binding.leaveChannel.setOnClickListener {
            leaveChannel()
        }
        binding.nameHeading.setOnClickListener {
            updateChannelName()
        }

    }

    private fun updateChannelName() {

        if (binding.channelName.text.toString().isEmpty()) return

        GlobalScope.launch(Dispatchers.IO) {
            if (channelClient.updatePartial(set = mapOf("name" to binding.channelName.text.toString())).execute().isError){
                val message = channelClient.updatePartial(set = mapOf("name" to binding.channelName.text.toString())).execute().error().message.toString()
                Log.d("update", message)
            }
        }

    }

    private fun leaveChannel() {

        channelClient.removeMembers(userId).enqueue{
            if (it.isSuccess) {
                activity?.onBackPressed()
                activity?.onBackPressed()
            } else {

            }
        }

    }

    private fun getChannelData() {

        channelClient.watch().enqueue {
            if (it.isSuccess) {
                channel = it.data()
                setRecycler()
                binding.channelName.setText(channel.extraData["name"].toString())
            } else {
                Toast.makeText(requireContext(), it.error().message.toString(), Toast.LENGTH_LONG).show()
            }
        }

    }

    private fun setRecycler() {

        binding.membersRecycler.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        binding.membersRecycler.layoutManager = layoutManager

        adapter = ChannelMembersAdapter(requireContext(), channel.members)
        binding.membersRecycler.adapter = adapter

    }

}

/*

[
    Member(
        user=User(
                id=588c6aadb00480fb2bfa47f5,
                role=user,
                invisible=false,
                banned=false,
                devices=[],
                online=false,
                createdAt=Thu Jan 13 20:30:57 GMT+05:30 2022,
                updatedAt=Thu Jan 13 21:24:45 GMT+05:30 2022,
                lastActive=Thu Jan 13 23:46:10 GMT+05:30 2022,
                totalUnreadCount=0,
                unreadChannels=0, mutes=[],
                teams=[],
                channelMutes=[],
                extraData={name=Harshit}
            ),
            role=owner,
            createdAt=Thu Jan 13 20:31:04 GMT+05:30 2022,
            updatedAt=Thu Jan 13 20:31:04 GMT+05:30 2022,
            isInvited=null,
            inviteAcceptedAt=null,
            inviteRejectedAt=null,
            shadowBanned=false
        ),
    Member(
        user=User(
                id=6112755c706eda0ef6275a89,
                role=user,
                invisible=false,
                banned=false,
                devices=[],
                online=true,
                createdAt=Thu Jan 13 15:09:51 GMT+05:30 2022,
                updatedAt=Thu Jan 13 15:09:51 GMT+05:30 2022,
                lastActive=Fri Jan 14 08:22:36 GMT+05:30 2022,
                totalUnreadCount=0,
                unreadChannels=0,
                mutes=[],
                teams=[],
                channelMutes=[],
                extraData={name=testing_employee}
            ),
            role=member,
            createdAt=Thu Jan 13 20:31:04 GMT+05:30 2022,
            updatedAt=Thu Jan 13 20:31:04 GMT+05:30 2022,
            isInvited=null,
            inviteAcceptedAt=null,
            inviteRejectedAt=null,
            shadowBanned=false
        )
]

 */