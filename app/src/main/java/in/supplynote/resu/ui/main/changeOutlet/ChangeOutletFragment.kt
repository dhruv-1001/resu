package `in`.supplynote.resu.ui.main.changeOutlet

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import `in`.supplynote.resu.R
import `in`.supplynote.resu.databinding.FragmentChangeOutletBinding
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class ChangeOutletFragment : Fragment() {

    private lateinit var binding: FragmentChangeOutletBinding

    private val viewModel: ChangeOutletViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view =  inflater.inflate(R.layout.fragment_change_outlet, container, false)
        binding = FragmentChangeOutletBinding.bind(view)



        return view

    }

}