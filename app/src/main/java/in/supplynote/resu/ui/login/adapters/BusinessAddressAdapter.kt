package `in`.supplynote.resu.ui.login.adapters

import `in`.supplynote.resu.R
import `in`.supplynote.resu.ui.login.BusinessAddressFragment
import `in`.supplynote.resu.ui.login.viewmodel.LoginViewModel
import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonArray
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class BusinessAddressAdapter(
    private var addressArray: JsonArray,
    private val businessAddressFragment: BusinessAddressFragment
): RecyclerView.Adapter<BusinessAddressAdapter.BusinessAddressViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BusinessAddressViewHolder {

        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_business_address, parent, false)
        return BusinessAddressViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: BusinessAddressViewHolder, position: Int) {

        holder.businessName.text = addressArray[position].asJsonObject.get("name").asString
        holder.businessAddress.text = addressArray[position].asJsonObject.get("formatted_address").asString
        holder.businessItemConst.setOnClickListener {
            businessAddressFragment.setFormattedAddressAndGotoAddressDetails(holder.businessName.text.toString(), holder.businessAddress.text.toString())
        }

    }

    override fun getItemCount(): Int {
        return addressArray.size()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateAddressList(newList: JsonArray){
        addressArray = newList
        notifyDataSetChanged()
    }

    class BusinessAddressViewHolder(view: View): RecyclerView.ViewHolder(view) {

        val businessName: TextView = view.findViewById(R.id.tv_business_name)
        val businessAddress: TextView = view.findViewById(R.id.tv_business_address)
        val businessItemConst: ConstraintLayout = view.findViewById(R.id.business_item_const)

    }

}
