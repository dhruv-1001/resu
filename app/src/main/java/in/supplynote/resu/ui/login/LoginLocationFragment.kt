package `in`.supplynote.resu.ui.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import `in`.supplynote.resu.R
import `in`.supplynote.resu.data.model.Status
import `in`.supplynote.resu.databinding.FragmentLoginLocationBinding
import `in`.supplynote.resu.ui.login.adapters.OutletAdapter
import `in`.supplynote.resu.ui.login.viewmodel.LoginViewModel
import `in`.supplynote.resu.ui.main.MainActivity
import `in`.supplynote.resu.utils.showToast
import android.app.AlertDialog
import android.content.Context
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class LoginLocationFragment : Fragment() {

    private lateinit var binding: FragmentLoginLocationBinding
    private val viewModel: LoginViewModel by activityViewModels()

    private var outletsList: JsonArray = JsonArray()
    private lateinit var outletAdapter: OutletAdapter


    private var selectedPosition = -1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_login_location, container, false)
        binding = FragmentLoginLocationBinding.bind(view)
        bindAndInit()
        return view
    }

    private fun bindAndInit() {

        binding.btContinue.setOnClickListener { gotoMain() }
        getOutletCall()

    }

    private fun getOutletCall() {

        viewModel.getOutlets().observe(requireActivity()) { res->
            when (res.status) {
                Status.LOADING -> {

                }
                Status.SUCCESS -> {
                    outletsList = res.data!!.asJsonArray
                    showToast("Setting up recycler")
                    setRecyclerView()
                }
                Status.ERROR -> {
                    showToast(res.msg.toString())
                }
            }
        }

    }

    private fun gotoMain() {

        if (selectedPosition == -1) {
            showToast("Select any Outlet")
            return
        }

        MainActivity.start(requireActivity())
        activity?.finish()

    }

    fun selectOutlet(pos: Int) {
        selectedPosition = pos
    }

    private fun setRecyclerView() {

        binding.rvLocations.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        binding.rvLocations.layoutManager = layoutManager
        outletAdapter = OutletAdapter(outletsList, this, requireContext())
        binding.rvLocations.adapter = outletAdapter
        outletAdapter.updateOutletList()

    }

    private fun showAlertToExit() {

        AlertDialog.Builder(requireContext())
            .setTitle("Closing Application")
            .setMessage("Are you sure you want to exit Resu?")
            .setPositiveButton("Yes") { _, _ -> activity?.finish() }
            .setNegativeButton("No", null)
            .show()

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                showAlertToExit()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }

}