package `in`.supplynote.resu.ui.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import `in`.supplynote.resu.R
import `in`.supplynote.resu.databinding.FragmentAddressDetailBinding
import `in`.supplynote.resu.ui.login.viewmodel.LoginViewModel
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class AddressDetailFragment : Fragment() {

    private lateinit var binding: FragmentAddressDetailBinding
    private lateinit var navController: NavController

    private val viewModel: LoginViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view =  inflater.inflate(R.layout.fragment_address_detail, container, false)
        binding = FragmentAddressDetailBinding.bind(view)

        bindAndInit()

        return view
    }

    private fun bindAndInit() {

        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment_container)
        binding.btNext.setOnClickListener{
            navigateToBusinessDetailsFragment()
        }

    }

    private fun navigateToBusinessDetailsFragment() {
        setFinalFormattedAddress()
        navController.navigate(R.id.action_addressDetailFragment_to_businessDetailsFragment)
        activity?.onBackPressed()
    }

    override fun onResume() {
        super.onResume()

        if (viewModel.getFormattedAddress().isNotEmpty()){
            getDetailsFromFormattedAddress(viewModel.getFormattedAddress())
        }

    }

    private fun setFinalFormattedAddress() {
        var address = ""
        address += binding.etAddressLine.text.toString() + ", "
        address += binding.etCity.text.toString() + ", "
        address += binding.etState.text.toString() + " "
        address += binding.etPinCode.text.toString() + ", "
        address += binding.etCountry.text.toString()

        viewModel.setFinalFormatterAddress(address)
    }

    private fun getDetailsFromFormattedAddress(addressStr: String) {

        binding.etBusinessName.setText(viewModel.getResultBusinessName())

        val address = addressStr.split(", ")
        val n = address.size

        binding.etCountry.setText(address[n-1])

        val stateAndPinStr = address[n-2]
        val stateAndPinList = stateAndPinStr.split(" ")
        val m = stateAndPinList.size

        binding.etPinCode.setText(stateAndPinList[m-1])

        var state = ""
        for (i in 0..(m-2)){
            state += stateAndPinList[i] + " "
        }

        state = state.dropLast(1)
        binding.etState.setText(state)

        binding.etCity.setText(address[n-3])

        var addressLine = ""
        for (i in 0..(n-4)) {
            addressLine += address[i] + ", "
        }
        addressLine = addressLine.dropLast(2)

        binding.etAddressLine.setText(addressLine)

    }


}