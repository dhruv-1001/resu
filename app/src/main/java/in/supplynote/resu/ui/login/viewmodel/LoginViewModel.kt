package `in`.supplynote.resu.ui.login.viewmodel

import `in`.supplynote.resu.data.model.Resource
import `in`.supplynote.resu.data.repository.GoogleRepository
import `in`.supplynote.resu.data.repository.NetworkRepository
import `in`.supplynote.resu.data.repository.UserDataRepository
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.google.gson.JsonObject
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class LoginViewModel @Inject constructor(
    private val repository: NetworkRepository,
    private val googleRepository: GoogleRepository
): ViewModel() {

    val receivedOtp = MutableLiveData<String>()

    private var phoneNumber = ""
    private var receivedOtpCodeFromFirebase = ""

    private var userId = ""

    private var userName = ""
    private var userToken = ""
    private var userMail = ""

    private var businessId = ""
    private var userExists = false

    private var invitedName = ""
    private var userInvited = false
    private var inviteAcceptObject = JsonObject()

    private var businessType = ""

    private var formattedAddress = ""
    private var finalFormatterAddress = ""

    private var resultBusinessName = ""
    val businessSearchText = MutableLiveData("")

    var isSupplyNoteUser = false

    fun setUserName(name: String) { userName = name }
    fun getUserName(): String { return userName }

    fun setUserMail(mail: String) { userMail = mail }
    fun getUserMail(): String { return userMail }

    fun setPhoneNum(number: String) { phoneNumber = number }
    fun getPhoneNum(): String { return phoneNumber }

    fun setUserId(id: String) { userId = id }
    fun getUserId(): String { return  userId }

    fun setOtpCodeFromFirebase(code: String) { receivedOtpCodeFromFirebase = code }
    fun getOtpCodeFromFirebase(): String { return receivedOtpCodeFromFirebase}

    fun setReceivedOtp(otp: String) { receivedOtp.value = otp }

    fun setBusinessType(type: String) { businessType = type }
    fun getBusinessType(): String { return businessType }

    fun setBusinessId(id: String) { businessId = id }

    fun setUserExists(state: Boolean) { userExists = state }
    fun getUserExists(): Boolean { return userExists }

    fun setUserInvited(state: Boolean) { userInvited = state }
    fun getUserInvited(): Boolean { return userInvited }

    fun setInviteAcceptObject(obj: JsonObject) { inviteAcceptObject = obj }
    fun getInviteAcceptObject():JsonObject { return inviteAcceptObject }

    fun setInvitedName(name: String) { invitedName = name }
    fun getInvitedName(): String { return invitedName }

    fun setResultBusinessName(name: String) { resultBusinessName = name }
    fun getResultBusinessName(): String { return resultBusinessName }

    fun setBusinessSearchText(text: String) { businessSearchText.value = text }

    fun setFormatterAddress(address: String) { formattedAddress = address }
    fun getFormattedAddress(): String { return formattedAddress }

    fun setFinalFormatterAddress(text: String) { finalFormatterAddress = text }
    fun getFinalFormatterAddress(): String { return finalFormatterAddress }

    fun setUserToken(token: String) { userToken = token }

    fun loginUser() {
        UserDataRepository.setBusinessId(businessId)
        UserDataRepository.setUserId(userId)
        UserDataRepository.setUserName(userName)
        UserDataRepository.setUserToken(userToken)
        UserDataRepository.setUserPhoneNumber("+91-$phoneNumber")
        UserDataRepository.loginUser()
    }

    fun checkUser() = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        try {
            val apiResponse = repository.checkUser("91-$phoneNumber")
            emit(Resource.success(apiResponse))
        } catch (e: java.lang.Exception) {
            emit(Resource.error(e.message.toString()))
        }
    }

    fun createUser(userJson: JsonObject) = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        try {
            val apiResponse = repository.createUser(userJson)
            emit(Resource.success(apiResponse))
        } catch (e: Exception) {
            emit(Resource.error(e.message.toString()))
        }

    }

    fun getUserInvite() = liveData(Dispatchers.IO) {

        emit(Resource.loading())
        try {
            val apiResponse = repository.getUserInvite("91-$phoneNumber")
            emit(Resource.success(apiResponse))
        } catch (e: Exception) {
            emit(Resource.error(e.message.toString()))
        }

    }

    fun acceptInvite() = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        try {
            val apiResponse = repository.acceptInvite(inviteAcceptObject)
            emit(Resource.success(apiResponse))
        } catch (e: Exception) {
            emit(Resource.error(e.message.toString()))
        }
    }

    fun loginWithSupplyNote(credentials: JsonObject) = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        try {
            val apiResponse = repository.login(credentials)
            emit(Resource.success(apiResponse))
        } catch (e: Exception) {
            if (e.message.toString() == "400")
                emit(Resource.error("Wrong Credentials"))
        }
    }

    fun updateUser(userJson: JsonObject) = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        try{
            val apiResponse = repository.updateUser(userJson)
            emit(Resource.success(apiResponse))
        } catch (e: Exception){
            emit(Resource.error(e.message.toString()))
        }
    }

    fun getLocations(searchText: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        try{
            val apiResponse = googleRepository.getLocations(searchText)
            emit(Resource.success(apiResponse))
        } catch (e: Exception){
            emit(Resource.error(e.message.toString()))
        }
    }

    fun createBusiness(businessJson: JsonObject) = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        try{
            val apiResponse = repository.createBusiness(businessJson)
            emit(Resource.success(apiResponse))
        } catch (e: Exception){
            emit(Resource.error(e.message.toString()))
        }
    }

    fun getOutlets() = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        try{
            val apiResponse = repository.getOutlets("JWT $userToken", businessId)
            emit(Resource.success(apiResponse))
        } catch (e: Exception){
            emit(Resource.error(e.message.toString()))
        }
    }

}