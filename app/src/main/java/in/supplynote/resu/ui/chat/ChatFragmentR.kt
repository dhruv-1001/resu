package `in`.supplynote.resu.ui.chat

import `in`.supplynote.resu.R
import `in`.supplynote.resu.databinding.FragmentChatBinding
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.getstream.sdk.chat.viewmodel.MessageInputViewModel
import com.getstream.sdk.chat.viewmodel.messages.MessageListViewModel
import io.getstream.chat.android.client.ChatClient
import io.getstream.chat.android.ui.message.input.viewmodel.bindView
import io.getstream.chat.android.ui.message.list.header.viewmodel.MessageListHeaderViewModel
import io.getstream.chat.android.ui.message.list.header.viewmodel.bindView
import io.getstream.chat.android.ui.message.list.viewmodel.bindView
import io.getstream.chat.android.ui.message.list.viewmodel.factory.MessageListViewModelFactory


class ChatFragmentR : Fragment() {

    private lateinit var binding: FragmentChatBinding
    private lateinit var cid: String

    private lateinit var client: ChatClient
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_chat_r, container,false)
        binding = FragmentChatBinding.bind(view)

        client = ChatClient.instance()

        val activity: ChatActivity = activity as ChatActivity
        cid = activity.getCid()

        bindAndInit()

        return view
    }

    private fun bindAndInit() {

        val factory = MessageListViewModelFactory(cid)
        val messageListHeaderViewModel: MessageListHeaderViewModel by viewModels { factory }
        val messageListViewModel: MessageListViewModel by viewModels { factory }
        val messageInputViewModel: MessageInputViewModel by viewModels { factory }

        messageListHeaderViewModel.bindView(binding.messageListHeaderView, this)
        messageListViewModel.bindView(binding.messageListView, this)
        messageInputViewModel.bindView(binding.messageInputView, this)

        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment_container)

        binding.messageListView.setMessageEditHandler(messageInputViewModel::postMessageToEdit)

        messageListViewModel.state.observe(viewLifecycleOwner) { state ->
            if (state is MessageListViewModel.State.NavigateUp) {
                activity?.finish()
            }
        }

        val backHandler = {
            messageListViewModel.onEvent(MessageListViewModel.Event.BackButtonPressed)
        }
        binding.messageListHeaderView.setBackButtonClickListener(backHandler)
        activity?.onBackPressedDispatcher?.addCallback(this) {
            backHandler()
        }
        binding.messageListHeaderView.setTitleClickListener{
            val bundle = bundleOf("cid" to cid)
            navController.navigate(R.id.action_chatFragment_to_editChannelFragment, bundle)
        }

    }

}