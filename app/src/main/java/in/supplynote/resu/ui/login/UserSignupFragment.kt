package `in`.supplynote.resu.ui.login

import `in`.supplynote.resu.R
import `in`.supplynote.resu.data.model.Status
import `in`.supplynote.resu.databinding.FragmentUserSignupBinding
import `in`.supplynote.resu.ui.login.viewmodel.LoginViewModel
import `in`.supplynote.resu.ui.main.MainActivity
import `in`.supplynote.resu.utils.animateView
import `in`.supplynote.resu.utils.showToast
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.OnBackPressedCallback
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class UserSignupFragment : Fragment() {

    private lateinit var binding: FragmentUserSignupBinding
    private lateinit var navController: NavController
    private val viewModel: LoginViewModel by activityViewModels()

    private val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_user_signup, container, false)
        binding = FragmentUserSignupBinding.bind(view)
        bindAndInitialise()
        return view

    }

    @SuppressLint("SetTextI18n")
    private fun bindAndInitialise() {

        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment_container)

        if (viewModel.getUserInvited()) {
            binding.etName.setText(viewModel.getInvitedName())
        }

        binding.tvPhoneNo.setText("+91 ${viewModel.getPhoneNum()}")

        binding.btNext.setOnClickListener {
            animateView(it)
            createUser()
        }

        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.designations,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.spDesignation.adapter = adapter
        }

        binding.etName.addTextChangedListener { clearErrors() }
        binding.etEmail.addTextChangedListener { clearErrors() }
        binding.spDesignation.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                clearErrors()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

    }

    private fun navigateToSelectBusiness() {
        navController.navigate(R.id.action_userSignupFragment_to_selectBusinessFragment)
    }

    private fun createUser() {

        if (!correctFields()) return

        val userJson = JsonObject()

        userJson.addProperty("name", binding.etName.text.toString())
        userJson.addProperty("email", binding.etEmail.text.toString())
        userJson.addProperty("phone", "+91-${viewModel.getPhoneNum()}")
        userJson.addProperty("profileImageURL", "")

        viewModel.createUser(userJson).observe(requireActivity()) { res ->

            when (res.status) {

                Status.LOADING -> {
                    lockUI()
                }
                Status.SUCCESS -> {
                    if (viewModel.getUserInvited()) {
                        createInviteObj(res.data!!)
                        acceptInvite()
                    } else if (viewModel.isSupplyNoteUser) {
                        viewModel.loginUser()
                        MainActivity.start(requireActivity())
                        activity?.finish()
                    } else {
                        getUserId(res.data!!)
                        navigateToSelectBusiness()
                    }
                }
                Status.ERROR -> {
                    unlockUI()
                    showToast(res.msg.toString())
                }
            }

        }
    }

    private fun getUserId(data: JsonElement) {
        val userId = data.asJsonObject.get("data").asJsonObject.get("_id").asString
        viewModel.setUserId(userId)
    }

    private fun correctFields(): Boolean {

        if (binding.etName.text.toString().isEmpty()){
            showNameError("*Enter your name")
            return false
        }
        if (binding.etEmail.text.toString().isEmpty()) {
            showEmailError("*Enter your Email")
            return false
        }
        if (!binding.etEmail.text.toString().matches(emailPattern.toRegex())){
            showEmailError("*Enter a valid Email")
            return false
        }
        if (binding.spDesignation.selectedItem.toString() == "Designation"){
            showDesignationError("*Select a designation")
            return false
        }

        return true
    }

    private fun showNameError(message: String){
        binding.nameError.text = message
    }

    private fun showEmailError(message: String){
        binding.mailError.text = message
    }

    private fun showDesignationError(message: String){
        binding.designationError.text = message
    }

    private fun clearErrors() {
        binding.nameError.text = ""
        binding.mailError.text = ""
        binding.designationError.text = ""
    }

    private fun createInviteObj(data: JsonElement) {

        val inviteAcceptObj = viewModel.getInviteAcceptObject()
        val userId = data.asJsonObject.get("data").asJsonObject.get("_id").asString
        viewModel.setUserId(userId)

        inviteAcceptObj.remove("user")
        inviteAcceptObj.addProperty("user", userId)

        viewModel.setInviteAcceptObject(inviteAcceptObj)

    }

    private fun acceptInvite() {

        viewModel.acceptInvite().observe(requireActivity()) { res ->
            when (res.status) {
                Status.LOADING -> {

                }
                Status.SUCCESS -> {
                    viewModel.loginUser()
                    showToast("Invite Accepted")
                    MainActivity.start(requireActivity())
                    activity?.finish()
                }
                Status.ERROR -> {
                    unlockUI()
                    showToast(res.msg.toString())
                }
            }
        }
    }

    private fun lockUI(){
        binding.pbLoading.visibility = View.VISIBLE
        binding.etName.isEnabled = false
        binding.etEmail.isEnabled = false
        binding.ivDesignationDrop.isEnabled = false
        binding.btNext.text = ""
        binding.btNext.isEnabled = false
    }

    private fun unlockUI() {
        binding.pbLoading.visibility = View.INVISIBLE
        binding.etName.isEnabled = true
        binding.etEmail.isEnabled = true
        binding.ivDesignationDrop.isEnabled = true
        binding.btNext.text = "NEXT"
        binding.btNext.isEnabled = false
    }

    private fun showAlertToExit() {

        AlertDialog.Builder(requireContext())
            .setTitle("Closing Application")
            .setMessage("Are you sure you want to exit Resu?")
            .setPositiveButton("Yes") { _, _ -> activity?.finish() }
            .setNegativeButton("No", null)
            .show()

    }

    override fun onResume() {
        super.onResume()

        if (viewModel.isSupplyNoteUser) {
            binding.etName.setText(viewModel.getUserName())
            binding.etEmail.setText(viewModel.getUserMail())
            binding.etName.isEnabled = false
            if (binding.etEmail.text.toString().isNotEmpty()){
                binding.etEmail.isEnabled = false
            }
        }

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                showAlertToExit()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }

}