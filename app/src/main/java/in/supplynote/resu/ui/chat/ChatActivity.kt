package `in`.supplynote.resu.ui.chat

import `in`.supplynote.resu.databinding.ActivityChatBinding
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import io.getstream.chat.android.client.models.Channel

class ChatActivity : AppCompatActivity() {

    private lateinit var binding: ActivityChatBinding

    private lateinit var cid: String
    private lateinit var userId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityChatBinding.inflate(layoutInflater)
        setContentView(binding.root)

        cid = checkNotNull(intent.getStringExtra(CID_KEY)) {
            "Specifying a channel id is required when starting ChannelActivity"
        }
        userId = checkNotNull(intent.getStringExtra(UID_KEY))


    }

    fun getCid(): String {
        return cid
    }

    fun getUid(): String {
        return userId
    }

    companion object {
        private const val CID_KEY = "key:cid"
        private const val UID_KEY = "key:userId"

        fun newIntent(context: Context, channel: Channel, userName: String): Intent =
            Intent(context, ChatActivity::class.java)
                .putExtra(CID_KEY, channel.cid)
                .putExtra(UID_KEY, userName)
    }

}