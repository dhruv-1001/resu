package `in`.supplynote.resu.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import `in`.supplynote.resu.R
import `in`.supplynote.resu.databinding.FragmentSupplierDetailBinding

class SupplierDetailFragment : Fragment() {

    private lateinit var binding: FragmentSupplierDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_add_item, container, false)
        binding = FragmentSupplierDetailBinding.bind(view)

        return view
    }

}