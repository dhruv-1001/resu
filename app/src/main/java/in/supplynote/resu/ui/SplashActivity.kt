package `in`.supplynote.resu.ui

import `in`.supplynote.resu.R
import `in`.supplynote.resu.data.repository.UserDataRepository
import `in`.supplynote.resu.ui.login.LoginActivity
import `in`.supplynote.resu.ui.main.MainActivity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import kotlinx.coroutines.*

@ExperimentalCoroutinesApi
@DelicateCoroutinesApi
class SplashActivity : AppCompatActivity() {

    private val SPLASH_TIME_OUT = 2000L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        GlobalScope.launch(Dispatchers.IO) {
            delay(SPLASH_TIME_OUT)
//            if (UserDataRepository.isUserLoggedIn()){
//                MainActivity.start(this@SplashActivity)
//                finish()
//            } else {
//                LoginActivity.start(this@SplashActivity)
//                finish()
//            }
            LoginActivity.start(this@SplashActivity)
            finish()
        }

    }
}