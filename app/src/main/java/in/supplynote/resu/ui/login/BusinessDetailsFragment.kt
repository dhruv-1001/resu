package `in`.supplynote.resu.ui.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import `in`.supplynote.resu.R
import `in`.supplynote.resu.data.model.Status
import `in`.supplynote.resu.databinding.FragmentBusinessDetailsBinding
import `in`.supplynote.resu.ui.login.viewmodel.LoginViewModel
import `in`.supplynote.resu.ui.main.MainActivity
import `in`.supplynote.resu.utils.animateView
import `in`.supplynote.resu.utils.showToast
import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.MediaStore
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.gson.JsonObject
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class BusinessDetailsFragment : Fragment() {

    private lateinit var binding: FragmentBusinessDetailsBinding
    private lateinit var navController: NavController

    private val viewModel: LoginViewModel by activityViewModels()

    private lateinit var country: String
    private lateinit var pin: String
    private lateinit var state: String
    private lateinit var city: String
    private lateinit var addressLine: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_business_details, container, false)
        binding = FragmentBusinessDetailsBinding.bind(view)

        bindAndInit()

        return view
    }

    private fun bindAndInit() {
        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment_container)

        binding.businessImage.setOnClickListener { getImageFromGallery() }

        binding.tvAddress.setOnClickListener { navigateToBusinessAddress() }

        binding.btFinish.setOnClickListener {
            animateView(it)
            createBusiness()
        }

        binding.ivBack.setOnClickListener { activity?.onBackPressed() }

        binding.etBusinessName.addTextChangedListener {
            viewModel.setBusinessSearchText(it.toString())
        }
    }

    private fun createBusiness() {

        if (!validInputs()) return

        val businessObject = JsonObject()
        businessObject.add("business", buildBusiness())
        businessObject.add("user", buildUser())
        businessObject.add("outletObj", buildOutlet())

        viewModel.createBusiness(businessObject).observe(requireActivity()) { res ->
            when(res.status){
                Status.LOADING -> {
                    lockUI()
                }
                Status.SUCCESS -> {
                    unlockUI()
                    MainActivity.start(requireActivity())
                    activity?.finish()
                }
                Status.ERROR -> {
                    unlockUI()
                    showToast(res.msg.toString())
                }
            }
        }

    }

    private fun validInputs(): Boolean {

        if (binding.etBusinessName.text.toString().isEmpty()) {
            showToast("Enter business name")
            return false
        }
        if (binding.etBrandName.text.toString().isEmpty()) {
            showToast("Enter brand name")
            return false
        }
        if (viewModel.getFormattedAddress() == ""){
            showToast("Select address")
            return false
        }
        if (binding.etGstNo.text.toString().isEmpty()) {
            showToast("Enter GST No name")
            return false
        }

        return true
    }

    private fun buildBusiness(): JsonObject {
        val obj = JsonObject()
        obj.addProperty("businessName", binding.etBusinessName.text.toString())
        obj.addProperty("brandName", binding.etBrandName.text.toString())
        obj.addProperty("businessCategory", viewModel.getBusinessType())
        obj.addProperty("gstin", binding.etGstNo.text.toString())
        return obj
    }

    private fun buildUser(): JsonObject {
        val obj = JsonObject()
        obj.addProperty("_id", viewModel.getUserId())
        obj.addProperty("userType", "user")
        return obj
    }

    private fun buildOutlet(): JsonObject {
        val obj = JsonObject()
        obj.addProperty("name", "")
        obj.addProperty("state", state)
        obj.addProperty("city", city)
        obj.addProperty("country", country)
        obj.addProperty("pincode", pin)
        obj.addProperty("AddressLine1", addressLine)
        return obj
    }

    private fun lockImage(){
        binding.businessImage.isEnabled = false
    }
    private fun unlockImage() {
        binding.businessImage.isEnabled = true
    }

    private fun lockUI() {
        lockImage()
        binding.btFinish.isEnabled = false
        binding.btFinish.text = ""
        binding.pbLoading.visibility = View.VISIBLE
    }

    private fun unlockUI() {
        unlockImage()
        binding.btFinish.isEnabled = true
        binding.btFinish.text = "FINISH"
        binding.pbLoading.visibility = View.INVISIBLE
    }

    private fun navigateToBusinessAddress() {
        navController.navigate(R.id.action_businessDetailsFragment_to_businessAddressFragment)
    }

    private fun getImageFromGallery() {

        lockImage()
        if(ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissionLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE)
        } else {
            startGallery()
        }
    }

    private val requestPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted: Boolean ->
        if (isGranted) {
            startGallery()
        } else {
            unlockImage()
            showToast("Permission Denied!")
        }
    }

    private fun startGallery() {

        val cameraIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startForResult.launch(cameraIntent)

    }

    private val startForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val intent = result.data
            val returnUri: Uri? = intent?.data
            val bitmapImage =
                MediaStore.Images.Media.getBitmap(requireActivity().contentResolver, returnUri)
            binding.businessImage.setImageBitmap(bitmapImage)
        }
    }

    override fun onPause() {
        super.onPause()
        lockImage()
    }

    override fun onResume() {
        super.onResume()
        unlockImage()
        if (viewModel.getFinalFormatterAddress().isNotEmpty()){
            getDetailsFromFormattedAddress(viewModel.getFinalFormatterAddress())
            binding.tvAddress.text = viewModel.getFinalFormatterAddress()
        }
    }

    private fun getDetailsFromFormattedAddress(addressStr: String) {

        val address = addressStr.split(", ")
        val n = address.size

        country = address[n-1]

        val stateAndPinStr = address[n-2]
        val stateAndPinList = stateAndPinStr.split(" ")
        val m = stateAndPinList.size

        pin = stateAndPinList[m-1]

        state = ""
        for (i in 0..(m-2)){
            state += stateAndPinList[i] + " "
        }

        state = state.dropLast(1)

        city = address[n-3]

        addressLine = ""
        for (i in 0..(n-4)) {
            addressLine += address[i] + ", "
        }

        addressLine = addressLine.dropLast(2)
    }

}