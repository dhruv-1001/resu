package `in`.supplynote.resu.ui.main

import `in`.supplynote.resu.R
import `in`.supplynote.resu.data.model.Status
import `in`.supplynote.resu.databinding.ActivityMainBinding
import `in`.supplynote.resu.ui.main.viewmodel.MainViewModel
import `in`.supplynote.resu.utils.showToast
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import dagger.hilt.android.AndroidEntryPoint
import io.getstream.chat.android.client.ChatClient
import io.getstream.chat.android.client.models.User
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, MainActivity::class.java))
        }
    }

    lateinit var userName: String
    lateinit var userId: String
    lateinit var businessId: String
    lateinit var userToken: String

    private lateinit var user: User

    private lateinit var navController: NavController
    private lateinit var binding: ActivityMainBinding

    private val client = ChatClient.instance()

    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        userName = viewModel.userName
        userId = viewModel.userId
        businessId = viewModel.businessId
        userToken = viewModel.userToken

        setupUser()
        bindAndInit()
        getBusinessDetails()

    }

    @SuppressLint("WrongConstant")
    private fun bindAndInit() {

        setSupportActionBar(binding.toolbar)
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment

        navController = navHostFragment.navController
        binding.bottomNavigation.setupWithNavController(navController)
        binding.drawerNavigation.setupWithNavController(navController)

        binding.toolbar.setNavigationOnClickListener {
            binding.drawerLayout.openDrawer(Gravity.START)
        }

        viewModel.showNavigationContent.observe(this) {
            if (it == true) {
                toggleNavigationContent(View.VISIBLE)
            } else  {
                toggleNavigationContent(View.GONE)
            }
        }

    }

    private fun toggleNavigationContent(visibility: Int){

        binding.bottomNavigation.visibility = visibility
        binding.drawerNavigation.visibility = visibility
        binding.toolbar.visibility = visibility

    }

    private fun getBusinessDetails() {

        viewModel.getBusinessData().observe(this) { res ->
            when (res.status) {
                Status.LOADING -> {
                    showToast("Loading Business")
                }
                Status.SUCCESS -> {
                    viewModel.setBusinessData(res.data!!)
                    binding.location.text = res.data.asJsonObject?.get("brandName")?.asString
                }
                Status.ERROR -> {
                    showToast(res.msg.toString())
                }
            }
        }
    }


    private fun setupUser() {
        user = User(
            id = userId,
            extraData = mutableMapOf(
                "name" to userName
            )
        )
        val token = client.devToken(userId)
        client.connectUser(
            user = user,
            token = token
        ).enqueue { result ->
            if (result.isSuccess){
                showToast("User Connected")
            } else {
                showToast(result.error().toString())
            }
        }
    }


}
