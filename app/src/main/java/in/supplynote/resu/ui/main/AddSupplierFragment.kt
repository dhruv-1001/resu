package `in`.supplynote.resu.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import `in`.supplynote.resu.R
import `in`.supplynote.resu.data.model.Resource
import `in`.supplynote.resu.data.model.Status
import `in`.supplynote.resu.databinding.FragmentAddSupplierBinding
import `in`.supplynote.resu.ui.main.viewmodel.MainViewModel
import `in`.supplynote.resu.utils.SnackbarLevel
import `in`.supplynote.resu.utils.d
import `in`.supplynote.resu.utils.fireSnackbar
import `in`.supplynote.resu.utils.showToast
import android.text.InputType
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import com.google.gson.JsonElement
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class AddSupplierFragment : Fragment() {

    private lateinit var binding: FragmentAddSupplierBinding
    private val viewModel: MainViewModel by activityViewModels()
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_add_supplier, container, false)
        binding = FragmentAddSupplierBinding.bind(view)
        return view

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupEditText()
        setupOnClickListeners()

    }

    private fun setupEditText() {

        binding.regBusinessName.apply {
            title.text = "Registered Business Name"
            input.hint = "Registered Business Name"
        }
        binding.brandName.apply {
            title.text = "Brand Name"
            input.hint = "Brand Name"
        }
        binding.mainAddress.apply {
            title.text = "Main Address"
            input.hint = "Main Address"
        }
        binding.phone.apply {
            title.text = "Phone"
            input.hint = "Phone"
            input.inputType = InputType.TYPE_CLASS_PHONE
        }
        binding.gstNumber.apply {
            title.text = "GST No"
            input.hint = "GST No"
        }

    }

    private fun setupOnClickListeners() {
        binding.btAddSupplier.setOnClickListener {
            onAddSupplierClick()
        }
    }

    private fun onAddSupplierClick() {
        if (incompleteInputFields()) {
            fireSnackbar(requireView(), SnackbarLevel.WARNING, "Input All Fields")
        }
        userCheck()
    }

    private fun userCheck() {

        viewModel.getUserData(binding.phone.input.text.toString()).observe(requireActivity()) { res ->
            when (res.status) {
                Status.LOADING -> {

                }
                Status.SUCCESS -> {
                    checkBusiness(res.data!!)
                }
                Status.ERROR -> {
                    d(res.msg.toString())
                }
            }
        }

    }

    private fun checkBusiness(user: JsonElement) {
        val userData = user.asJsonObject
        if (!userData.get("data").asJsonObject.get("user").asJsonObject.get("buisness").isJsonNull){
            handleBusinessExistCase()
            return
        }
        addSupplier()
    }

    private fun handleBusinessExistCase() {

        fireSnackbar(requireView(), SnackbarLevel.INFO, "Business Already Exists")

    }

    private fun addSupplier() {



    }

    private fun incompleteInputFields(): Boolean {

        if (binding.regBusinessName.input.text.isNullOrEmpty()) return true
        if (binding.brandName.input.text.isNullOrEmpty()) return true
        if (binding.mainAddress.input.text.isNullOrEmpty()) return true
        if (binding.phone.input.text.isNullOrEmpty()) return true
        if (binding.gstNumber.input.text.isNullOrEmpty()) return true

        return false
    }

}