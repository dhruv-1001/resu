package `in`.supplynote.resu.ui.main.adapters

import `in`.supplynote.resu.R
import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonArray

class MembersShowAdapter(
    private val membersArray: JsonArray
): RecyclerView.Adapter<MembersShowAdapter.MemberViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MemberViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_member, parent, false)
        return MemberViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MemberViewHolder, position: Int) {
        holder.memberName.text = membersArray[position].asJsonObject.get("name").asString
        holder.memberPhone.text = membersArray[position].asJsonObject.get("phone").asString
        holder.nameInitial.text = membersArray[position].asJsonObject.get("name").asString.toString()[0].uppercaseChar().toString()
        if (membersArray[position].asJsonObject.get("buisness").asJsonObject.get("businessCategory").asString == "buyer") {
            holder.businessType.text = "Restaurant"
        } else {
            holder.businessType.text = "Supplier"
        }
    }

    override fun getItemCount(): Int {
        return membersArray.size()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateRecycler() {
        notifyDataSetChanged()
    }

    class MemberViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val memberName: TextView = view.findViewById(R.id.member_name)
        val memberPhone: TextView = view.findViewById(R.id.member_phone_number)
        val nameInitial: TextView = view.findViewById(R.id.name_initial)
        val businessType: TextView = view.findViewById(R.id.member_business_type)
    }

}