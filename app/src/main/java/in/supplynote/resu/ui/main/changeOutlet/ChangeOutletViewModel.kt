package `in`.supplynote.resu.ui.main.changeOutlet

import `in`.supplynote.resu.data.repository.NetworkRepository
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class ChangeOutletViewModel @Inject constructor(
    private val repository: NetworkRepository
): ViewModel() {



}