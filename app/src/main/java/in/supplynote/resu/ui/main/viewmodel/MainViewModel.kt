package `in`.supplynote.resu.ui.main.viewmodel

import `in`.supplynote.resu.data.model.Resource
import `in`.supplynote.resu.data.repository.NetworkRepository
import `in`.supplynote.resu.data.repository.UserDataRepository
import `in`.supplynote.resu.utils.BusinessType
import `in`.supplynote.resu.utils.d
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.lang.Exception
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: NetworkRepository
): ViewModel(){

    val userName = UserDataRepository.getUserName()
    val userId = UserDataRepository.getUserId()
    val businessId = UserDataRepository.getBusinessId()
    val phoneNumber = UserDataRepository.getUserPhoneNumber()
    val userToken = UserDataRepository.getUserToken()

    val showNavigationContent = MutableLiveData(false)

    private val businessData = MutableLiveData<JsonElement>()
    val teamBusinessMembers = MutableLiveData<JsonElement>()
    val businessMembersJsonArray = MutableLiveData<JsonArray>()
    val selectedTeamMembersIds = MutableLiveData(ArrayList<String>())

    val otherBusinessMembers = MutableLiveData<JsonElement>()
    val otherBusinessMembersJsonArray = MutableLiveData<JsonArray>()
    val selectedOtherMembersIds = MutableLiveData(ArrayList<String>())

    var businessType: BusinessType = BusinessType.RESTAURANT

    var teamMembersSelected = false
    var otherMembersSelected = false

    var isTeamMembersData = true
    private var selectedOtherBusinessId = ""

    fun setSelectedOtherBusinessId(_id: String) {
        selectedOtherBusinessId = _id
    }
    fun getSelectedOtherBusinessId(): String {
        return selectedOtherBusinessId
    }

    fun hideNavigationContent() {
        showNavigationContent.value = false
    }

    fun showNavigationContent() {
        showNavigationContent.value = true
    }

    fun setBusinessData(data: JsonElement) {
        businessData.value = data
        extractData()
    }

    fun setTeamBusinessUsers(data: JsonElement) {
        teamBusinessMembers.value = data
    }

    fun setOtherBusinessUsers(data: JsonElement) {
        otherBusinessMembers.value = data
    }

    private fun extractData() {

        val businessObj = businessData.value!!.asJsonObject
        val businessTypeString = businessObj.get("businessCategory").asString
        businessType = if (businessTypeString == "buyer") {
            BusinessType.RESTAURANT
        } else {
            BusinessType.SUPPLIER
        }

    }

    fun getSelectedTeamMembersJson(): JsonArray {

        val selectedJson = JsonArray()
        for (x in selectedTeamMembersIds.value!!) {
            for (y in businessMembersJsonArray.value!!) {
                val userID = y.asJsonObject.get("_id").asString
                if (x == userID) {
                    selectedJson.add(y)
                    break
                }
            }
        }
        return selectedJson
    }

    fun getSelectedOtherMembersJson(): JsonArray {
        val selectedJson = JsonArray()
        for (x in selectedOtherMembersIds.value!!) {
            for (y in otherBusinessMembersJsonArray.value!!) {
                val userID = y.asJsonObject.get("_id").asString
                if (x == userID) {
                    selectedJson.add(y)
                    break
                }
            }
        }
        return selectedJson
    }

    fun setupModifyTeamMemberJsonArray() {

        val jsonArray = teamBusinessMembers.value!!.asJsonArray
        val modifiedJsonArray = JsonArray()

        for (x in jsonArray) {
            if (x.asJsonObject.get("_id").asString != userId) {
                if (x.asJsonObject.has("isResuAppUser")) {
                    if (x.asJsonObject.get("isResuAppUser").asBoolean){
                        modifiedJsonArray.add(x.asJsonObject)
                    }
                }
            }
        }
        businessMembersJsonArray.value = modifiedJsonArray

    }

    fun setupModifyOtherMemberJsonArray() {
        val jsonArray = otherBusinessMembers.value!!.asJsonArray
        val modifiedJsonArray = JsonArray()
        for (x in jsonArray) {
            d(x.toString())
            if (x.asJsonObject.has("isResuAppUser")) {
                d("have isResu")
                if (x.asJsonObject.get("isResuAppUser").asBoolean){
                    d(x.asJsonObject.get("isResuAppUser").asBoolean.toString())
                    modifiedJsonArray.add(x.asJsonObject)
                }
            }
        }
        otherBusinessMembersJsonArray.value = modifiedJsonArray
    }

    fun getBusinessArrayList(type: String): ArrayList<String>{

        val businessDataJson = businessData.value!!.asJsonObject
        val businessArrayList: ArrayList<String> = ArrayList()

        val businesses: JsonArray = businessDataJson.get(type).asJsonArray
        for (x in businesses) {
            if (x.asJsonObject.get("_id").asString != businessId) {
                businessArrayList.add(x.asJsonObject.get("businessName").asString)
            }
        }
        return businessArrayList
    }

    fun getBusinessJsonArray(type: String): JsonArray {
        val businessDataJson = businessData.value!!.asJsonObject
        val businessJsonArray = JsonArray()

        val businesses: JsonArray = businessDataJson.get(type).asJsonArray
        for (x in businesses) {
            if (x.asJsonObject.get("_id").asString != businessId) {
                businessJsonArray.add(x)
            }
        }
        return businessJsonArray
    }

    fun getBusinessData() = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        try {
            val apiResponse = repository.getBusinessData("JWT $userToken", businessId)
            emit(Resource.success(apiResponse))
        } catch (e: Exception) {
            emit(Resource.error(e.message.toString()))
        }
    }

    fun getTeamBusinessUsers() = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        try {
            val apiResponse = repository.getBusinessUsers("JWT $userToken", businessId)
            emit(Resource.success(apiResponse))
        } catch (e: Exception) {
            emit(Resource.error(e.message.toString()))
        }
    }

    fun getOtherBusinessUsers() = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        try {
            val apiResource = repository.getBusinessUsers("JWT $userToken", selectedOtherBusinessId)
            emit(Resource.success(apiResource))
        } catch (e: Exception) {
            emit(Resource.error(e.message.toString()))
        }
    }

    fun getUserData(phone: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        try {
            val apiResponse = repository.checkUser("91-$phone")
            emit(Resource.success(apiResponse))
        } catch (e: Exception) {
            emit(Resource.error(e.message.toString()))
        }
    }

}