package `in`.supplynote.resu.ui.main

import `in`.supplynote.resu.R
import `in`.supplynote.resu.data.model.Status
import `in`.supplynote.resu.databinding.FragmentAddMemberBinding
import `in`.supplynote.resu.ui.main.adapters.MembersAdapter
import `in`.supplynote.resu.ui.main.adapters.SelectedMembersAdapter
import `in`.supplynote.resu.ui.main.viewmodel.MainViewModel
import `in`.supplynote.resu.utils.showToast
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class AddMemberFragment : Fragment() {

    private lateinit var binding: FragmentAddMemberBinding
    private lateinit var membersAdapter: MembersAdapter
    private lateinit var selectedMembersAdapter: SelectedMembersAdapter

    private val viewModel: MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_add_member, container, false)
        binding = FragmentAddMemberBinding.bind(view)

        bindAndInit()

        return view
    }

    private fun bindAndInit() {

        binding.ivBack.setOnClickListener {
            activity?.onBackPressed()
        }

        if (viewModel.isTeamMembersData){
            getTeamBusinessUsers()
        } else {
            getOtherBusinessUsers()
        }

        viewModel.selectedTeamMembersIds.observe(requireActivity()) {
            if (it.size == 0) {
                binding.rvSelectedMembers.visibility = View.GONE
            } else {
                binding.rvSelectedMembers.visibility = View.VISIBLE
//                updateSelectedRecycler()
            }
        }

        binding.btContinue.setOnClickListener {
            if (viewModel.isTeamMembersData) {
                viewModel.teamMembersSelected = true
            } else {
                viewModel.otherMembersSelected = true
            }
            binding.ivBack.performClick()
        }

    }

    private fun getOtherBusinessUsers() {

        if (viewModel.otherBusinessMembers.value != null) {
            setupOtherMemberRecycler()
            setupSelectedOtherMemberRecycler()
            return
        }

        viewModel.getOtherBusinessUsers().observe(requireActivity()) { res ->
            when (res.status){
                Status.LOADING -> {

                }
                Status.SUCCESS -> {
                    viewModel.setOtherBusinessUsers(res.data!!)
                    viewModel.setupModifyOtherMemberJsonArray()
                    setupOtherMemberRecycler()
                    setupSelectedOtherMemberRecycler()
                }
                Status.ERROR -> {
                    showToast(res.msg.toString())
                }
            }
        }

    }

    private fun getTeamBusinessUsers() {

        if (viewModel.teamBusinessMembers.value != null) {
            setupMemberRecycler()
            setupSelectedMemberRecycler()
            return
        }

        viewModel.getTeamBusinessUsers().observe(requireActivity()) { res ->
            when (res.status) {
                Status.LOADING -> {

                }
                Status.SUCCESS -> {
                    viewModel.setTeamBusinessUsers(res.data!!)
                    viewModel.setupModifyTeamMemberJsonArray()
                    setupMemberRecycler()
                    setupSelectedMemberRecycler()
                }
                Status.ERROR -> {
                    showToast(res.msg.toString())
                }
            }
        }
    }

    private val selectMember: (selectedArray: ArrayList<String>) -> Unit = {
        viewModel.selectedTeamMembersIds.value = it
        selectedMembersAdapter.updateSelectedList(viewModel.getSelectedTeamMembersJson())
    }

    private val removeMember: (selectedArray: ArrayList<String>) -> Unit = {
        viewModel.selectedTeamMembersIds.value = it
        selectedMembersAdapter.updateSelectedList(viewModel.getSelectedTeamMembersJson())
    }

    private val removeMemberFromSelected: (_id: String) -> Unit = {
        viewModel.selectedTeamMembersIds.value!!.remove(it)
        membersAdapter.updateSelectedList(viewModel.selectedTeamMembersIds.value!!)
        selectedMembersAdapter.updateSelectedList(viewModel.getSelectedTeamMembersJson())
    }

    private val selectOtherMember: (selectedArray: ArrayList<String>) -> Unit = {
        viewModel.selectedOtherMembersIds.value = it
        selectedMembersAdapter.updateSelectedList(viewModel.getSelectedOtherMembersJson())
    }

    private val removeOtherMember: (selectedArray: ArrayList<String>) -> Unit = {
        viewModel.selectedOtherMembersIds.value = it
        selectedMembersAdapter.updateSelectedList(viewModel.getSelectedOtherMembersJson())
    }

    private val removeOtherMemberFromSelected: (_id: String) -> Unit = {
        viewModel.selectedOtherMembersIds.value!!.remove(it)
        membersAdapter.updateSelectedList(viewModel.selectedOtherMembersIds.value!!)
        selectedMembersAdapter.updateSelectedList(viewModel.getSelectedOtherMembersJson())
    }

    private fun setupOtherMemberRecycler() {
        binding.rvMembers.setHasFixedSize(true)
        membersAdapter = MembersAdapter(
            viewModel.otherBusinessMembersJsonArray.value!!,
            viewModel.selectedOtherMembersIds.value!!,
            selectOtherMember,
            removeOtherMember
        )
        binding.rvMembers.adapter = membersAdapter
    }

    private fun setupSelectedOtherMemberRecycler() {

        binding.rvSelectedMembers.setHasFixedSize(true)
        selectedMembersAdapter = SelectedMembersAdapter(
            viewModel.getSelectedOtherMembersJson(),
            removeOtherMemberFromSelected
        )
        binding.rvSelectedMembers.adapter = selectedMembersAdapter
        selectedMembersAdapter.updateRecycler()

    }

    private fun setupMemberRecycler() {

        binding.rvMembers.setHasFixedSize(true)
        membersAdapter = MembersAdapter(
            viewModel.businessMembersJsonArray.value!!,
            viewModel.selectedTeamMembersIds.value!!,
            selectMember,
            removeMember
        )
        binding.rvMembers.adapter = membersAdapter
        membersAdapter.updateRecycler()

    }

    private fun setupSelectedMemberRecycler() {

        binding.rvSelectedMembers.setHasFixedSize(true)
        selectedMembersAdapter = SelectedMembersAdapter(
            viewModel.getSelectedTeamMembersJson(),
            removeMemberFromSelected,
        )
        binding.rvSelectedMembers.adapter = selectedMembersAdapter
        selectedMembersAdapter.updateRecycler()

    }

}