package `in`.supplynote.resu.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import `in`.supplynote.resu.R
import `in`.supplynote.resu.databinding.FragmentSettingsBinding
import `in`.supplynote.resu.ui.main.viewmodel.MainViewModel
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class SettingsFragment : Fragment() {

    lateinit var binding: FragmentSettingsBinding
    private lateinit var navController: NavController
    private val viewModel: MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_settings, container, false)
        binding = FragmentSettingsBinding.bind(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupOnClickListeners()

    }

    private fun setupOnClickListeners() {

        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)

        binding.addSupplier.setOnClickListener {
            navigateToAddSupplier()
        }

    }

    private fun navigateToAddSupplier() {
        navController.navigate(R.id.action_settingsFragment_to_addSupplierFragment)
        viewModel.hideNavigationContent()
    }

}