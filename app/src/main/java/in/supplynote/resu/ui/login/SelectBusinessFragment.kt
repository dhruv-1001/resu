package `in`.supplynote.resu.ui.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import `in`.supplynote.resu.R
import `in`.supplynote.resu.databinding.FragmentSelectBusinessBinding
import `in`.supplynote.resu.ui.login.viewmodel.LoginViewModel
import `in`.supplynote.resu.utils.animateView
import `in`.supplynote.resu.utils.showToast
import android.app.AlertDialog
import android.content.Context
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class SelectBusinessFragment : Fragment() {

    private lateinit var binding: FragmentSelectBusinessBinding
    private lateinit var navController: NavController

    private lateinit var phoneNumber: String

    private val viewModel: LoginViewModel by activityViewModels()
    private var businessSelected = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_select_business, container, false)
        binding = FragmentSelectBusinessBinding.bind(view)
        bindAndInitialise()
        return view

    }

    private fun bindAndInitialise() {

        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment_container)

        binding.proceedRe.setOnClickListener {
            animateView(it)
            selectBuyer()
        }

        binding.proceedSu.setOnClickListener {
            animateView(it)
            selectSeller()
        }

        binding.btNext.setOnClickListener { navigateToProfileSignUp() }

    }

    private fun selectBuyer() {
        viewModel.setBusinessType("buyer")
        binding.proceedRe.background = ContextCompat.getDrawable(requireContext(), R.drawable.card_dark_background)
        binding.reCheck.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_resu_check_selected))

        binding.proceedSu.background = ContextCompat.getDrawable(requireContext(), R.drawable.card_light_background)
        binding.suCheck.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_resu_check))
        businessSelected = true
    }

    private fun selectSeller() {
        viewModel.setBusinessType("seller")
        binding.proceedSu.background = ContextCompat.getDrawable(requireContext(), R.drawable.card_dark_background)
        binding.suCheck.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_resu_check_selected))

        binding.proceedRe.background = ContextCompat.getDrawable(requireContext(), R.drawable.card_light_background)
        binding.reCheck.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_resu_check))
        businessSelected = true
    }

    private fun navigateToProfileSignUp() {
        if (!businessSelected){
            showToast("Please Select Your Business Type")
            return
        }
        navController.navigate(R.id.action_selectBusinessFragment_to_businessDetailsFragment)
    }

    private fun showAlertToExit() {

        AlertDialog.Builder(requireContext())
            .setTitle("Close Application")
            .setMessage("Are you sure you want to exit Resu?")
            .setPositiveButton("Yes") { _, _ -> activity?.finish() }
            .setNegativeButton("No", null)
            .show()

    }

    override fun onResume() {
        super.onResume()

        when (viewModel.getBusinessType()) {
            "buyer" -> {
                selectBuyer()
            }
            "seller" -> {
                selectSeller()
            }
            "" -> {}
        }

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                showAlertToExit()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }

}