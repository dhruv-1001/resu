package `in`.supplynote.resu.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import `in`.supplynote.resu.R
import `in`.supplynote.resu.databinding.FragmentCreatePurchaseBinding
import `in`.supplynote.resu.ui.main.viewmodel.MainViewModel
import `in`.supplynote.resu.utils.showToast
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class CreatePurchaseFragment : Fragment() {

    private lateinit var binding: FragmentCreatePurchaseBinding
    private val viewModel: MainViewModel by viewModels()
    private lateinit var businessId: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentCreatePurchaseBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        businessId = requireArguments().getString("businessId", "")
        showToast(businessId)

    }

}