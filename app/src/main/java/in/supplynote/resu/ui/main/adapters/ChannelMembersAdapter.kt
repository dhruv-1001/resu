package `in`.supplynote.resu.ui.main.adapters

import `in`.supplynote.resu.R
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import io.getstream.chat.android.client.models.Member

class ChannelMembersAdapter(
    private val context: Context,
    private val membersList: List<Member>
) : RecyclerView.Adapter<ChannelMembersAdapter.MyViewHolder>() {

//    private lateinit var membersList: List<Member>
//    private lateinit var context: Context

//    fun ChannelMembersAdapter(context: Context, membersList: List<Member>) {
//
//        this.context = context
//        this.membersList = membersList
//
//    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_channel_members, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val memberName = membersList[position].user.extraData["name"].toString()
        holder.userName.text = memberName

    }

    override fun getItemCount(): Int {
        return membersList.size
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var constraintLayout: ConstraintLayout = view.findViewById(R.id.member_list_const)
        var userName: TextView = view.findViewById(R.id.user_name)

    }

}