package `in`.supplynote.resu.ui.login

import `in`.supplynote.resu.R
import `in`.supplynote.resu.data.model.Status
import `in`.supplynote.resu.databinding.FragmentOtpBinding
import `in`.supplynote.resu.ui.login.viewmodel.LoginViewModel
import `in`.supplynote.resu.ui.main.MainActivity
import `in`.supplynote.resu.utils.animateView
import `in`.supplynote.resu.utils.d
import `in`.supplynote.resu.utils.hideKeyboard
import `in`.supplynote.resu.utils.showToast
import android.annotation.SuppressLint
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.util.concurrent.TimeUnit

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class OtpFragment : Fragment() {

    private lateinit var binding: FragmentOtpBinding
    private lateinit var codeReceived: String
    private lateinit var navController: NavController
    private lateinit var phoneNumber: String

    private var auth: FirebaseAuth = Firebase.auth

    private val viewModel: LoginViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_otp, container, false)
        binding = FragmentOtpBinding.bind(view)
        bindAndInitialise()
        return view

    }

    private val timer = object : CountDownTimer(59000, 1000) {
        override fun onTick(p0: Long) {
            updateTimeText((p0/1000).toInt())
        }
        override fun onFinish() {
            binding.tvResentOtp.visibility = View.VISIBLE
        }
    }

    @SuppressLint("SetTextI18n")
    private fun bindAndInitialise() {

        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment_container)

        codeReceived = viewModel.getOtpCodeFromFirebase()
        phoneNumber = viewModel.getPhoneNum()

        binding.tvPhoneNo.text = resources.getString(R.string.country_code)+ " " + phoneNumber

        binding.btVerify.setOnClickListener{
            hideKeyboard()
            animateView(it)
            verify()
        }

        timer.start()
        binding.tvResentOtp.setOnClickListener {
            sendOtp(viewModel.getPhoneNum())
        }

        binding.ivBack.setOnClickListener { activity?.onBackPressed() }

        viewModel.receivedOtp.observe(requireActivity()) {
            binding.etActualOtp.setText(it)
            binding.btVerify.performClick()
        }

        // HAHA ik this is evil and total "jugaad"
        binding.etActualOtp.addTextChangedListener {
            clearOTPError()
            setPointer()
            try {
                val enteredStr = it.toString()
                binding.tvOtp1.text = ""
                binding.tvOtp1.text = enteredStr[0].toString()
                binding.tvOtp2.text = ""
                binding.tvOtp2.text = enteredStr[1].toString()
                binding.tvOtp3.text = ""
                binding.tvOtp3.text = enteredStr[2].toString()
                binding.tvOtp4.text = ""
                binding.tvOtp4.text = enteredStr[3].toString()
                binding.tvOtp5.text = ""
                binding.tvOtp5.text = enteredStr[4].toString()
                binding.tvOtp6.text = ""
                binding.tvOtp6.text = enteredStr[5].toString()
            } catch (e: Exception) {

            }
        }

        setPointer()

        binding.etActualOtp.setOnClickListener {
            val pos = binding.etActualOtp.text.toString().length
            binding.etActualOtp.setSelection(pos)
        }

    }

    private fun setPointer() {
        when (binding.etActualOtp.text.toString().length) {
            0 -> {
                selectPointer(binding.tvOtp1)
                removePointer(binding.tvOtp2)
            }
            1 -> {
                removePointer(binding.tvOtp1)
                selectPointer(binding.tvOtp2)
                removePointer(binding.tvOtp3)
            }
            2 -> {
                removePointer(binding.tvOtp2)
                selectPointer(binding.tvOtp3)
                removePointer(binding.tvOtp4)
            }
            3 -> {
                removePointer(binding.tvOtp3)
                selectPointer(binding.tvOtp4)
                removePointer(binding.tvOtp5)
            }
            4 -> {
                removePointer(binding.tvOtp4)
                selectPointer(binding.tvOtp5)
                removePointer(binding.tvOtp6)
            }
            5 -> {
                removePointer(binding.tvOtp5)
                selectPointer(binding.tvOtp6)
            }
        }
    }

    private fun selectPointer(view: TextView) {
        view.background = ContextCompat.getDrawable(requireContext(), R.drawable.card_light_background)
    }

    private fun removePointer(view: TextView) {
        view.background = ContextCompat.getDrawable(requireContext(), R.drawable.edit_text_background)
    }

    private fun lockUI() {
        binding.pbLoading.visibility = View.VISIBLE
        binding.btVerify.text = ""
        binding.btVerify.isEnabled = false
        binding.etActualOtp.isEnabled = false
    }

    private fun unlockUI() {
        binding.pbLoading.visibility = View.INVISIBLE
        binding.btVerify.text = "CONTINUE"
        binding.btVerify.isEnabled = true
        binding.etActualOtp.isEnabled = true
    }

    private fun showOTPError(message: String) {
        binding.tvErrorOtp.text = message
    }
    private fun clearOTPError() {
        binding.tvErrorOtp.text = ""
    }

    private var callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        override fun onCodeSent(p0: String, p1: PhoneAuthProvider.ForceResendingToken) {
            super.onCodeSent(p0, p1)
            timer.start()
            viewModel.setOtpCodeFromFirebase(p0)
        }

        override fun onVerificationCompleted(p0: PhoneAuthCredential) {}

        override fun onVerificationFailed(p0: FirebaseException) {}

    }

    private fun sendOtp(number: String) {

        val options = PhoneAuthOptions.newBuilder(auth)
            .setPhoneNumber("+91$number")
            .setTimeout(0L, TimeUnit.SECONDS)
            .setActivity(requireActivity())
            .setCallbacks(callbacks)
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)

    }

    private fun checkOtpLength() : Boolean {

        if (binding.etActualOtp.text.toString().length == 6) return true
        showOTPError("*Enter a valid OTP")
        return false

    }

    private fun verify() {

        if (!checkOtpLength()) return

        lockUI()

        val enteredOtp = binding.etActualOtp.text.toString()
        val credential = PhoneAuthProvider.getCredential(codeReceived, enteredOtp)

        FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener {
            if (it.isSuccessful){
                checkExistingUser()
            }
        }.addOnFailureListener {
            unlockUI()
            showOTPError("*Wrong OTP")
            showToast(it.message.toString(), Toast.LENGTH_LONG)
        }

    }

    private fun checkExistingUser() {

        viewModel.checkUser().observe(requireActivity()) { res ->
            when (res.status) {
                Status.LOADING -> {
                    // progress bar working
                }
                Status.SUCCESS -> {
                    d("Checking user state")
                    checkUserState(res.data!!)
                }
                Status.ERROR -> {
                    if (res.msg.toString() == "400") {
                        checkInvite()
                    } else {
                        unlockUI()
                        showToast(res.msg.toString())
                    }
                }
            }
        }

    }


    private fun checkUserState(data: JsonElement) {

        val userId = data.asJsonObject.get("data").asJsonObject.get("user").asJsonObject.get("_id").asString
        var businessId: String? = null
        var isResuAppUser = false
        d("here 1")
        if (!data.asJsonObject.get("data").asJsonObject.get("user").asJsonObject.get("buisness").isJsonNull) {
            d("here 2")
            businessId = data.asJsonObject.get("data").asJsonObject.get("user").asJsonObject.get("buisness").asString
            d("here 3")
        }

        if (data.asJsonObject.get("data").asJsonObject.get("user").asJsonObject.has("isResuAppUser")) {
            d("here 4")
            isResuAppUser = data.asJsonObject.get("data").asJsonObject.get("user").asJsonObject.get("isResuAppUser").asBoolean
            d("here 5")
        }

        val userToken = data.asJsonObject.get("data").asJsonObject.get("user").asJsonObject.get("token").asString
        val userName = data.asJsonObject.get("data").asJsonObject.get("user").asJsonObject.get("name").asString
        var userMail = ""
        if (data.asJsonObject.get("data").asJsonObject.get("user").asJsonObject.has("email")) {
            userMail = data.asJsonObject.get("data").asJsonObject.get("user").asJsonObject.get("email").asString
        }
        d("here 5")
        viewModel.setUserExists(true)
        viewModel.setUserId(userId)
        viewModel.setUserName(userName)
        viewModel.setUserMail(userMail)
        d("here 6")
        if (businessId != null) {
            d("here 7")
            viewModel.setBusinessId(businessId)
            d("here 8")
        }

        when {
            isResuAppUser -> {
                d("Navigating to main menu")
                viewModel.setUserToken(userToken)
                viewModel.loginUser()

                MainActivity.start(requireActivity())
                activity?.finish()

            }
            !businessId.isNullOrEmpty() -> {
                d("Navigating to create user")
                viewModel.isSupplyNoteUser = true
                navigateToCreateUser()

            }
            else -> {
                d("Checking invite")
                checkInvite()
            }
        }
    }

    private fun checkInvite() {
        d("checking invite")
        viewModel.getUserInvite().observe(requireActivity()) { res ->
            when (res.status) {
                Status.LOADING -> {
                    // progress bar working
                }
                Status.SUCCESS -> {
                    checkIfInvited(res.data!!)
                }
                Status.ERROR -> {
                    unlockUI()
                    showToast(res.msg.toString())
                }
            }
        }

    }

    private fun checkIfInvited(data: JsonElement) {

        val inviteData = data.asJsonObject.get("data").asJsonArray

        if (inviteData.size() == 0) {
            if (viewModel.getUserExists()){
                navigateToCreateBusiness()
            } else {
                navigateToCreateUser()
            }
        } else {
            viewModel.setUserInvited(true)
            viewModel.setInvitedName(inviteData[0].asJsonObject.get("name").asString)
            if (viewModel.getUserExists()){
                createAcceptInviteObject(inviteData[0])
                navigateToInvite()
            } else {
                createAcceptInviteObjectWithoutUid(inviteData[0])
                navigateToInvite()
            }
        }
    }

    private fun navigateToInvite() {
        navController.navigate(R.id.action_otpFragment_to_inviteFragment)
    }

    private fun navigateToCreateBusiness() {
        navController.navigate(R.id.action_otpFragment_to_selectBusinessFragment)
    }

    private fun navigateToCreateUser() {
        navController.navigate(R.id.action_otpFragment_to_userSignupFragment)
    }

    private fun navigateToSelectLocation() {
        navController.navigate(R.id.action_otpFragment_to_loginLocationFragment)
    }

    private fun createAcceptInviteObjectWithoutUid(data: JsonElement) {

        val inviteId = data.asJsonObject.get("_id").asString
        val businessId = data.asJsonObject.get("user").asJsonObject.get("buisness").asString

        val inviteAcceptObject = JsonObject()
        val invitedUser = JsonObject()

        invitedUser.addProperty("_id", inviteId)
        invitedUser.addProperty("business", businessId)

        inviteAcceptObject.add("invitedUser", invitedUser)
        inviteAcceptObject.addProperty("user", "")

        viewModel.setInviteAcceptObject(inviteAcceptObject)
        viewModel.setBusinessId(businessId)

    }

    private fun createAcceptInviteObject(data: JsonElement) {

        val inviteId = data.asJsonObject.get("_id").asString
        val businessId = data.asJsonObject.get("user").asJsonObject.get("buisness").asString
        val userId = viewModel.getUserId()


        val inviteAcceptObject = JsonObject()
        val invitedUser = JsonObject()

        invitedUser.addProperty("_id", inviteId)
        invitedUser.addProperty("business", businessId)

        inviteAcceptObject.add("invitedUser", invitedUser)
        inviteAcceptObject.addProperty("user", userId)

        viewModel.setInviteAcceptObject(inviteAcceptObject)
        viewModel.setBusinessId(businessId)

    }

    private fun updateTimeText(secondsRemaining: Int){

        var secondsToShow = "00:"
        secondsToShow += if (secondsRemaining < 10) "0$secondsRemaining"
        else "$secondsRemaining"

        binding.tvTimer.text = secondsToShow

    }

}