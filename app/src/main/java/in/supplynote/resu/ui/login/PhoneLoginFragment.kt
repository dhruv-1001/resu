package `in`.supplynote.resu.ui.login

import `in`.supplynote.resu.R
import `in`.supplynote.resu.databinding.FragmentPhoneLoginBinding
import `in`.supplynote.resu.ui.login.viewmodel.LoginViewModel
import `in`.supplynote.resu.utils.animateView
import `in`.supplynote.resu.utils.showToast
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.util.concurrent.TimeUnit


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class PhoneLoginFragment : Fragment() {

    private lateinit var binding: FragmentPhoneLoginBinding
    private lateinit var navController: NavController

    private var auth: FirebaseAuth = Firebase.auth

    private val viewModel: LoginViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_phone_login, container, false)
        binding = FragmentPhoneLoginBinding.bind(view)
        bindAndInit()
        return view

    }

    private fun bindAndInit() {

        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment_container)

        binding.etPhoneNum.addTextChangedListener {
            binding.tvError.visibility = View.INVISIBLE
        }

        binding.btSendOtp.setOnClickListener{
            animateView(it)
            if (checkPhoneNum()) {
                lockUI()
                sendOtp(binding.etPhoneNum.text.toString())
            }
        }

        binding.ivBack.setOnClickListener { activity?.onBackPressed() }
        binding.etPhoneNum.requestFocus()

    }

    private fun lockUI() {
        binding.pbSendingOtp.visibility = View.VISIBLE
        binding.btSendOtp.text = ""
        binding.btSendOtp.isEnabled = false
        binding.etPhoneNum.isEnabled = false
    }

    private fun unlockUI() {
        binding.pbSendingOtp.visibility = View.INVISIBLE
        binding.btSendOtp.text = "SEND OTP"
        binding.btSendOtp.isEnabled = true
        binding.etPhoneNum.isEnabled = true
    }

    private fun navigateToOtpFragment(){
        navController.navigate(R.id.action_phoneLoginFragment_to_otpFragment)
    }

    private var callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        override fun onCodeSent(p0: String, p1: PhoneAuthProvider.ForceResendingToken) {
            super.onCodeSent(p0, p1)

            viewModel.setPhoneNum(binding.etPhoneNum.text.toString())
            viewModel.setOtpCodeFromFirebase(p0)

            navigateToOtpFragment()
        }

        override fun onVerificationCompleted(p0: PhoneAuthCredential) {}

        override fun onVerificationFailed(p0: FirebaseException) {
            unlockUI()
            showToast(p0.message.toString())
        }

    }

    private fun sendOtp(number: String) {

        val options = PhoneAuthOptions.newBuilder(auth)
            .setPhoneNumber("+91$number")
            .setTimeout(0L, TimeUnit.SECONDS)
            .setActivity(requireActivity())
            .setCallbacks(callbacks)
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)

    }

    private fun checkPhoneNum(): Boolean {
        if (binding.etPhoneNum.text.toString().length == 10) return true
        binding.tvError.visibility = View.VISIBLE
        return false
    }

}