package `in`.supplynote.resu.ui.login.adapters

import `in`.supplynote.resu.R
import `in`.supplynote.resu.ui.login.LoginLocationFragment
import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonArray
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class OutletAdapter(
    private var outletArray: JsonArray,
    private val loginLocationFragment: LoginLocationFragment,
    private val context: Context
): RecyclerView.Adapter<OutletAdapter.LoginLocationViewHolder>() {

    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LoginLocationViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_outlet, parent, false)
        return LoginLocationViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: LoginLocationViewHolder, position: Int) {

        holder.outletName.text = outletArray[position].asJsonObject.get("name").asString
        if (selectedPosition == position) {
            holder.outletConst.background = ContextCompat.getDrawable(context, R.drawable.card_light_background)
            holder.checkIcon.visibility = View.VISIBLE
        } else {
            holder.outletConst.background = ContextCompat.getDrawable(context, R.drawable.card_white_background)
            holder.checkIcon.visibility = View.INVISIBLE
        }
        holder.outletConst.setOnClickListener {
            selectedPosition = position
            updateOutletList()
        }

    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateOutletList() {
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return outletArray.size()
    }

    class LoginLocationViewHolder(view: View): RecyclerView.ViewHolder(view) {

        val outletName: TextView = view.findViewById(R.id.outlet_name)
        val checkIcon: ImageView = view.findViewById(R.id.outlet_list_check)
        val outletConst: ConstraintLayout = view.findViewById(R.id.outlet_const)

    }

}