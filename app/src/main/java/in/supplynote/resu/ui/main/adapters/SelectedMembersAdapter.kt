package `in`.supplynote.resu.ui.main.adapters

import `in`.supplynote.resu.R
import `in`.supplynote.resu.ui.main.AddMemberFragment
import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonArray
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class SelectedMembersAdapter(
    private var selectedMembersJson: JsonArray,
    private var removeMemberFromSelected: (_id: String) -> Unit,
): RecyclerView.Adapter<SelectedMembersAdapter.SelectedMembersViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectedMembersViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_selected_member, parent, false)
        return SelectedMembersViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: SelectedMembersViewHolder, position: Int) {

        holder.memberName.text = selectedMembersJson[position].asJsonObject.get("name").asString
        holder.nameInitial.text = selectedMembersJson[position].asJsonObject.get("name").asString.toString()[0].uppercaseChar().toString()

        val userId = selectedMembersJson[position].asJsonObject.get("_id").asString

        holder.selectedMemberConst.setOnClickListener {
            removeMemberFromSelected(userId)
        }

    }

    override fun getItemCount(): Int {
        return selectedMembersJson.size()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateRecycler() {
        notifyDataSetChanged()
    }

    fun updateSelectedList(selectedArray: JsonArray) {
        this.selectedMembersJson = selectedArray
        updateRecycler()
    }

    class SelectedMembersViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val memberName: TextView = view.findViewById(R.id.member_name)
        val nameInitial: TextView = view.findViewById(R.id.name_initial)
        val selectedMemberConst: ConstraintLayout = view.findViewById(R.id.item_selected_member_const)
    }
}