package `in`.supplynote.resu.ui.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import `in`.supplynote.resu.R
import `in`.supplynote.resu.data.model.Status
import `in`.supplynote.resu.databinding.FragmentBusinessAddressBinding
import `in`.supplynote.resu.ui.login.adapters.BusinessAddressAdapter
import `in`.supplynote.resu.ui.login.viewmodel.LoginViewModel
import `in`.supplynote.resu.utils.showToast
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class BusinessAddressFragment : Fragment() {

    private lateinit var binding: FragmentBusinessAddressBinding
    private val viewModel: LoginViewModel by activityViewModels()

    private lateinit var businessAddressAdapter: BusinessAddressAdapter
    private lateinit var navController: NavController

    private var businessAddressList: JsonArray = JsonArray()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_business_address, container, false)
        binding = FragmentBusinessAddressBinding.bind(view)
        bindAndInit()
        return view
    }

    private fun bindAndInit() {

        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment_container)

        binding.etSearch.requestFocus()
        binding.ivBack.setOnClickListener {
            activity?.onBackPressed()
        }
        binding.etSearch.addTextChangedListener {
            if (binding.etSearch.text.toString().isNotEmpty()) {
                binding.ivCross.visibility = View.VISIBLE
            } else {
                binding.ivCross.visibility = View.INVISIBLE
            }
            getNewLocations(it.toString())
        }

        binding.ivCross.setOnClickListener { binding.etSearch.setText("") }

        binding.tvManualEnter.setOnClickListener {
            setFormattedAddressAndGotoAddressDetails("", "")
        }

        setRecyclerView()

        if (viewModel.businessSearchText.value.toString() != ""){
            binding.etSearch.setText(viewModel.businessSearchText.value.toString())
            viewModel.setBusinessSearchText("")
        }

    }

    private fun setRecyclerView() {
        binding.rvBusinessAddress.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        binding.rvBusinessAddress.layoutManager = layoutManager
        businessAddressAdapter = BusinessAddressAdapter(businessAddressList, this)
        binding.rvBusinessAddress.adapter = businessAddressAdapter
    }

    private fun getNewLocations(searchText: String) {

        if (searchText.isEmpty()) {
            businessAddressList = JsonArray()
            businessAddressAdapter.updateAddressList(businessAddressList)
            binding.manualConst.visibility = View.VISIBLE
            return
        }
        viewModel.getLocations(searchText).observe(requireActivity()) { res ->
            when(res.status){
                Status.SUCCESS -> {
                    updateRecycler(res.data!!)
                }
                Status.ERROR -> {
                    showToast(res.msg.toString())
                }
            }
        }
    }

    private fun updateRecycler(data: JsonElement) {

        businessAddressList = data.asJsonObject.get("results").asJsonArray
        if (businessAddressList.size() == 0) {
            businessAddressAdapter.updateAddressList(businessAddressList)
            binding.manualConst.visibility = View.VISIBLE
        }
        else if (businessAddressList.size() > 5){
            binding.manualConst.visibility = View.INVISIBLE
            businessAddressList = takeFirstFive(businessAddressList)
            businessAddressAdapter.updateAddressList(businessAddressList)
        } else {
            binding.manualConst.visibility = View.INVISIBLE
            businessAddressAdapter.updateAddressList(businessAddressList)
        }

    }

    private fun takeFirstFive(data: JsonArray): JsonArray {
        val newJsonArray = JsonArray()
        for (i in 0..4) {
            newJsonArray.add(data[i])
        }
        return newJsonArray
    }

    fun setFormattedAddressAndGotoAddressDetails(businessName: String, address: String) {
        viewModel.setResultBusinessName(businessName)
        viewModel.setFormatterAddress(address)
        navigateToAddressDetails()
    }

    private fun navigateToAddressDetails() {
        navController.navigate(R.id.action_businessAddressFragment_to_addressDetailFragment)
    }


}