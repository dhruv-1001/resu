package `in`.supplynote.resu.ui.main

import `in`.supplynote.resu.R
import `in`.supplynote.resu.databinding.FragmentChannelBinding
import `in`.supplynote.resu.ui.main.viewmodel.MainViewModel
import `in`.supplynote.resu.utils.showToast
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import dagger.hilt.android.AndroidEntryPoint
import io.getstream.chat.android.client.ChatClient
import io.getstream.chat.android.client.models.Filters
import io.getstream.chat.android.client.models.User
import io.getstream.chat.android.ui.channel.list.viewmodel.ChannelListViewModel
import io.getstream.chat.android.ui.channel.list.viewmodel.bindView
import io.getstream.chat.android.ui.channel.list.viewmodel.factory.ChannelListViewModelFactory
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class ChannelFragment : Fragment() {

    lateinit var binding: FragmentChannelBinding

    private lateinit var client: ChatClient
    private lateinit var user: User

    private lateinit var userId: String

    private val viewModel: MainViewModel by activityViewModels()
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_channel, container, false)
        binding = FragmentChannelBinding.bind(view)

        client = ChatClient.instance()
        user = client.getCurrentUser()!!

        userId = viewModel.userId

        fetchChannels()
        bindAndInit()

        return view

    }

    private fun bindAndInit() {
        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)
        binding.createChannel.setOnClickListener { navigateToCreateChannel() }
    }

    private fun navigateToCreateChannel() {
        navController.navigate(R.id.action_channelFragment_to_createChannelFragment)
        viewModel.hideNavigationContent()
    }

    private fun navigateToChatFragment(cid: String) {
        val bundle = Bundle()
        bundle.putString("cid", cid)
        navController.navigate(R.id.action_channelFragment_to_chatFragment, bundle)
        viewModel.hideNavigationContent()
    }

    private fun fetchChannels() {

        val filter = Filters.and(
            Filters.eq("type", "messaging"),
            Filters.`in`("members", listOf(user.id))
        )
        val viewModelFactory = ChannelListViewModelFactory(filter, ChannelListViewModel.DEFAULT_SORT)
        val channelListViewModel: ChannelListViewModel by viewModels { viewModelFactory }

        channelListViewModel.bindView(binding.channelListView, this)
        binding.channelListView.setChannelItemClickListener { channel ->
            navigateToChatFragment(channel.cid)
        }

    }

    private fun makeChannel() {

        showToast("Creating Channel")

        client.createChannel(
            channelType = "messaging",
            members = listOf("6112755c706eda0ef6275a89", "6202b95db1685015326007a8", "620a569e3bc2e73d0679f4fe"),
            extraData = mutableMapOf(
                "name" to "SupplyNote - Setup3"
            ),
        ).enqueue{
            if (it.isSuccess) {
                showToast("Channel Created")
            } else {
                showToast(it.error().toString())
            }
        }
    }

    override fun onResume() {
        super.onResume()

        viewModel.showNavigationContent()

    }

}