package `in`.supplynote.resu.ui.main.adapters

import `in`.supplynote.resu.R
import `in`.supplynote.resu.ui.main.AddMemberFragment
import `in`.supplynote.resu.utils.animateAndHide
import `in`.supplynote.resu.utils.animateAndShow
import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonArray
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class MembersAdapter(
    private val membersArray: JsonArray,
    private var selectedIds: ArrayList<String>,
    private val selectMember: (selectedArray: ArrayList<String>) -> Unit,
    private val removeMember: (selectedArray: ArrayList<String>) -> Unit
): RecyclerView.Adapter<MembersAdapter.MemberViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MemberViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_member, parent, false)
        return MemberViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MemberViewHolder, position: Int) {

        holder.memberName.text = membersArray[position].asJsonObject.get("name").asString
        holder.memberPhone.text = membersArray[position].asJsonObject.get("phone").asString
        holder.nameInitial.text = membersArray[position].asJsonObject.get("name").asString.toString()[0].uppercaseChar().toString()
        if (membersArray[position].asJsonObject.get("buisness").asJsonObject.get("businessCategory").asString == "buyer") {
            holder.businessType.text = "Restaurant"
        } else {
            holder.businessType.text = "Supplier"
        }

        val userId = membersArray[position].asJsonObject.get("_id").asString

        if (selectedIds.contains(userId)) {
            animateAndShow(holder.checked)
        } else {
            animateAndHide(holder.checked)
        }

        holder.membersItemConst.setOnClickListener {
            if (selectedIds.contains(userId)){
                selectedIds.remove(userId)
                removeMember(selectedIds)
                animateAndHide(holder.checked)
            } else {
                selectedIds.add(userId)
                selectMember(selectedIds)
                animateAndShow(holder.checked)
            }
        }

    }

    override fun getItemCount(): Int {
        return membersArray.size()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateRecycler() {
        notifyDataSetChanged()
    }

    fun updateSelectedList(selectedArray: ArrayList<String>) {
        this.selectedIds = selectedArray
        updateRecycler()
    }

    class MemberViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val memberName: TextView = view.findViewById(R.id.member_name)
        val memberPhone: TextView = view.findViewById(R.id.member_phone_number)
        val nameInitial: TextView = view.findViewById(R.id.name_initial)
        val membersItemConst: ConstraintLayout = view.findViewById(R.id.item_member_const)
        val checked: ImageView = view.findViewById(R.id.checked)
        val businessType: TextView = view.findViewById(R.id.member_business_type)
    }

}
