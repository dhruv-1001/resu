package `in`.supplynote.resu

import `in`.supplynote.resu.data.repository.UserDataRepository
import android.app.Application
import android.content.Context
import dagger.hilt.android.HiltAndroidApp
import io.getstream.chat.android.client.ChatClient
import io.getstream.chat.android.client.logger.ChatLogLevel
import io.getstream.chat.android.livedata.ChatDomain

@HiltAndroidApp
class ResuApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        UserDataRepository.setSharedPreferences(applicationContext.getSharedPreferences("user_info", Context.MODE_PRIVATE))

        val client =
            ChatClient.Builder(getString(R.string.api_key), this).logLevel(ChatLogLevel.ALL).build()
        ChatDomain.Builder(client, this).build()

    }
}