package `in`.supplynote.resu.utils

import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment

fun Fragment.showToast(message: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(requireContext(), message, duration).show()
}

fun Fragment.hideKeyboard(){
    view?.let { activity?.hideKeyboard(it) }
}

fun Fragment.animateView(view: View) {

    view.animate().apply {
        duration = 100
        scaleX(0.9f)
        scaleY(0.9f)
    }.withEndAction {
        view.animate().apply {
            duration = 100
            scaleX(1f)
            scaleY(1f)
        }
    }

}

fun animateAndHide(view: View) {
    view.animate().apply {
        duration = 100
        scaleY(0f)
        scaleX(0f)
    }.withEndAction {
        view.visibility = View.GONE
    }
}

fun animateAndShow(view: View) {
    view.visibility = View.VISIBLE
    view.animate().apply {
        duration = 100
        scaleY(1f)
        scaleX(1f)
    }
}

fun shrink(view: View) {
    view.animate().apply {
        duration = 100
        scaleX(0f)
        scaleY(0f)
    }
}