package `in`.supplynote.resu.utils

class CONSTANTS {

    companion object{

//        const val BASE_URL = "https://dev.supplynote.in/api/"
//        const val BASE_URL = "https://www.supplynote.in/api/"
        const val BASE_URL = "http://15.206.123.5:3000/api/"
        const val GOOGLE_MAPS_BASE_URL = "https://maps.googleapis.com/maps/api/"

        const val REQ_USER_CONSENT = 200

    }

}