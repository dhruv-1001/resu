package `in`.supplynote.resu.utils

import android.util.Log

fun d(message: String, tag: String = "testing") {
    Log.d(tag, message)
}