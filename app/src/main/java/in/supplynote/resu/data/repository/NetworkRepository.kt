package `in`.supplynote.resu.data.repository

import `in`.supplynote.resu.data.api.NetworkService
import `in`.supplynote.resu.data.api.SafeApiRequest
import android.util.Log
import com.google.gson.JsonObject
import javax.inject.Inject

class NetworkRepository @Inject constructor(
    private val networkApi: NetworkService
): SafeApiRequest (){

    // -------------- SupplyNote Calls ------------------

    suspend fun login(credentials: JsonObject) = apiRequest {
        networkApi.login(credentials)
    }

    suspend fun getBusinessData(token: String, businessId: String) = apiRequest {
        networkApi.getBusinessData(token, businessId)
    }

    suspend fun getOutlets(token: String, businessId: String) = apiRequest {
        networkApi.getOutlets(token, businessId)
    }

    suspend fun getBusinessUsers(token: String, businessId: String) = apiRequest {
        networkApi.getBusinessUsers(token, businessId)
    }

    // -------------- Resu app calls --------------------

    suspend fun createUser(userJson: JsonObject) = apiRequest {
        networkApi.createUser(userJson)
    }

    suspend fun checkUser(phoneNumber: String) = apiRequest {
        networkApi.checkUser(phoneNumber)
    }

    suspend fun getUserInvite(phoneNumber: String) = apiRequest {
        networkApi.getUserInvite(phoneNumber)
    }

    suspend fun acceptInvite(acceptInviteObject: JsonObject) = apiRequest {
        networkApi.acceptInvite(acceptInviteObject)
    }

    suspend fun updateUser(userJson: JsonObject) = apiRequest {
        networkApi.updateUser(userJson)
    }

    suspend fun createBusiness(businessJson: JsonObject) = apiRequest {
        networkApi.businessCreate(businessJson)
    }

}