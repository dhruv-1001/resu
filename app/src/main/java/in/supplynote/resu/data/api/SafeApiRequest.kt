package `in`.supplynote.resu.data.api

import `in`.supplynote.resu.utils.d
import retrofit2.Response

abstract class SafeApiRequest {

    suspend fun <T: Any> apiRequest(call : suspend() -> Response<T>): T {

        d("Calling Api")
        val response = call.invoke()
        d("Api Called")
        if (response.isSuccessful && response.body() != null) {
            d("Call Success")
            d("Call Data: ${response.body().toString()}")
            return response.body()!!
        }
        else {
            d("Call Failed")
            throw Exception(response.code().toString())
        }

    }

}