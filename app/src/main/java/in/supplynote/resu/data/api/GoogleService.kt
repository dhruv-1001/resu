package `in`.supplynote.resu.data.api

import com.google.gson.JsonElement
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface GoogleService {

    @GET("place/textsearch/json")
    suspend fun getLocations(
        @Query("query") searchText: String,
        @Query("key") apiKey: String
    ): Response<JsonElement>

}