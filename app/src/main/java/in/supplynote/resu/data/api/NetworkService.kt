package `in`.supplynote.resu.data.api

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface NetworkService {

    @Headers("Accept: application/json", "Content-Type: application/json")

    // ----------------- SupplyNote apis --------------------

    @POST("auth/signin")
    suspend fun login(
        @Body credentials: JsonObject
    ): Response<JsonElement>


    @GET("buisnesses/{business_id}")
    suspend fun getBusinessData(
        @Header("Authorization") token: String,
        @Path("business_id") businessId: String
    ): Response<JsonElement>

    @GET("outlets")
    suspend fun getOutlets(
        @Header("Authorization") token: String,
        @Query("buisness") businessId: String
    ): Response<JsonElement>

    @GET("buisnesses/user/{business_id}")
    suspend fun getBusinessUsers(
        @Header("Authorization") token: String,
        @Path("business_id") businessId: String
    ): Response<JsonElement>

    // ----------------- Resu Apis --------------------------

    @GET("resu-apps/user-exists")
    suspend fun checkUser(
        @Query("phone") number: String
    ): Response<JsonElement>


    @POST("resu-apps/user-create")
    suspend fun createUser(
        @Body userJson: JsonObject
    ) : Response<JsonElement>

    @GET("resu-apps/get-user-invite")
    suspend fun getUserInvite(
        @Query("phone") number: String
    ): Response<JsonElement>

    @POST("resu-apps/invite-accept")
    suspend fun acceptInvite(
        @Body acceptInviteObject: JsonObject
    ): Response<JsonElement>

    @POST("resu-apps/invite-user")
    suspend fun inviteUser(
        @Body inviteJson: JsonObject
    ): Response<JsonElement>

    @POST("resu-apps/business-create")
    suspend fun businessCreate(
        @Body businessJson: JsonObject
    ): Response<JsonElement>

    @PUT("resu-apps/user-update")
    suspend fun updateUser(
        @Body userJson: JsonObject
    ): Response<JsonElement>

}