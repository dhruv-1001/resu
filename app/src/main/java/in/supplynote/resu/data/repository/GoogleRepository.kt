package `in`.supplynote.resu.data.repository

import `in`.supplynote.resu.data.api.GoogleService
import `in`.supplynote.resu.data.api.SafeApiRequest
import javax.inject.Inject

class GoogleRepository @Inject constructor(
    private val googleRepository: GoogleService
): SafeApiRequest() {

    suspend fun getLocations(searchText: String) = apiRequest {
        googleRepository.getLocations(searchText, "AIzaSyDDfgerDVQX4gdaNHkzeHqsGa0xcpsA8qo")
    }

}