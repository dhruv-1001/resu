package `in`.supplynote.resu.data.repository

import android.content.SharedPreferences

object UserDataRepository {

    private lateinit var sharedPreferences: SharedPreferences
    fun setSharedPreferences(sharedPref: SharedPreferences) {
        sharedPreferences = sharedPref
    }

    fun loginUser() {
        val editor = sharedPreferences.edit()
        editor.putBoolean("logged_in", true)
        editor.apply()
    }

    fun logoutUser() {
        val editor = sharedPreferences.edit()
        editor.putBoolean("logged_in", false)
        editor.apply()
    }

    fun isUserLoggedIn(): Boolean {
        return sharedPreferences.getBoolean("logged_in", false)
    }

    fun setUserName(name: String) {
        val editor = sharedPreferences.edit()
        editor.putString("user_name", name)
        editor.apply()
    }

    fun getUserName(): String {
        return sharedPreferences.getString("user_name", "")!!
    }
    fun setUserId(_id: String) {
        val editor = sharedPreferences.edit()
        editor.putString("user_id", _id)
        editor.apply()
    }

    fun getUserId(): String {
        return sharedPreferences.getString("user_id", "")!!
    }

    fun setBusinessId(_id: String) {
        val editor = sharedPreferences.edit()
        editor.putString("business_id", _id)
        editor.apply()
    }

    fun getBusinessId(): String {
        return sharedPreferences.getString("business_id", "")!!
    }

    fun setUserToken(token: String) {
        val editor = sharedPreferences.edit()
        editor.putString("user_token", token)
        editor.apply()
    }

    fun setUserPhoneNumber(number: String) {
        val editor = sharedPreferences.edit()
        editor.putString("user_phone_number", number)
        editor.apply()
    }

    fun getUserPhoneNumber(): String {
        return sharedPreferences.getString("user_phone_number", "")!!
    }

    fun getUserToken(): String {
        return sharedPreferences.getString("user_token", "")!!
    }

}