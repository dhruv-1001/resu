package `in`.supplynote.resu.di.module

import `in`.supplynote.resu.data.api.GoogleService
import `in`.supplynote.resu.utils.CONSTANTS
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class MapsModule {

    private val okHttpClient: OkHttpClient.Builder = OkHttpClient.Builder()
        .connectTimeout(120, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS)
        .writeTimeout(120, TimeUnit.SECONDS)

    @Provides
    @Singleton
    operator fun invoke() : GoogleService {

        return Retrofit.Builder()
            .baseUrl(CONSTANTS.GOOGLE_MAPS_BASE_URL)
            .client(okHttpClient.build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(GoogleService::class.java)

    }

}